/**
 * 
 */
package graphic;

import graphic.cards.ImageFactory;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

/**
 * @author Sissoko
 * @date 30 avr. 2014 22:31:38
 */
@SuppressWarnings("serial")
public class MessagePanel extends JPanel {

	static int imageSize = 75;
	static int padding = 5;
	BufferedImage image;
	JTextPane content;
	JLabel nameLabel;

	String name;
	String message;

	/**
	 * @param name
	 * @param messge
	 */
	public MessagePanel(String name, String message) {
		this.name = name;
		this.message = message;
		setLayout(null);
		initComponents();
	}

	private void initComponents() {
		int width = imageSize + 320;
		int height = 100;
		setSize(width, height);
		image = ImageFactory.getBufferedImage("computer.png");
		nameLabel = new JLabel();
		nameLabel.setSize(width - imageSize - 2 * padding, 30);
		nameLabel.setForeground(Color.BLUE);
		nameLabel.setFont(new Font("Times New Roman", Font.BOLD, 15));
		nameLabel.setText(name);
		nameLabel.setLocation(imageSize + 2 * padding, padding);
		content = new JTextPane();
		content.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		content.setText(message);
		// JScrollPane contentScoll = new JScrollPane();
		// contentScoll.setViewportView(content);
		content.setSize(width - imageSize - 2 * padding,
				height - nameLabel.getHeight() - 2 * padding);
		content.setEditable(false);
		content.setBackground(getBackground());
		setBorder(BorderFactory.createTitledBorder(""));
		content.setLocation(imageSize + 2 * padding, nameLabel.getY()
				+ nameLabel.getHeight());
		add(nameLabel);
		add(content);
		setOpaque(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.Container#paintComponents(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		int ox = padding;
		int oy = padding;
		int width = imageSize;
		int height = imageSize;
		Polygon polygon = new Polygon(new int[] { ox, ox + width, ox + width,
				ox + width, ox },
				new int[] { oy, oy, oy + height, oy + height }, 4);
		TexturePaint texture = new TexturePaint(image, polygon.getBounds());
		g2d.setPaint(texture);
		g2d.fillOval(ox, oy, width, height);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame f = new JFrame();
				f.setLayout(null);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				MessagePanel mp = new MessagePanel(
						"Cheick Mahady SISSOKO",
						"Ceci est un message de la part de Cheick\nSi tu n'es pas d'accord c'est ton probleme.\n"
								+ "Ceci est un message de la part de Cheick\nSi tu n'es pas d'accord c'est ton probleme.\n"
								+ "Ceci est un message de la part de Cheick\nSi tu n'es pas d'accord c'est ton probleme.");
				f.add(mp);
				mp.setLocation(10, 10);
				f.setBounds(100, 100, 400, 200);
				f.setVisible(true);
			}
		});

	}

}
