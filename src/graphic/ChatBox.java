/**
 * 
 */
package graphic;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

/**
 * @author Sissoko
 * @date 30 avr. 2014 22:23:51
 */
@SuppressWarnings("serial")
public class ChatBox extends JPanel {

	JScrollPane scrollPanel;
	JPanel content;
	String name;
	int lastX = 0;
	int lastY = 0;
	JTextPane messageText;

	public interface Listener {
		void onSend(String name, String message);
	}

	Listener listener;

	/**
	 * @param name
	 */
	public ChatBox(String name) {
		this.name = name;
		initComponents();
	}

	/**
	 * 
	 */
	public ChatBox() {
		this("Vous");
	}

	private void initComponents() {
		setLayout(null);
		int width = 410;
		int height = 475;
		setSize(width, height);
		scrollPanel = new JScrollPane();
		add(scrollPanel);
		scrollPanel.setLocation(0, 0);
		content = new JPanel();
		content.setLayout(null);
		scrollPanel.setSize(width, 410);
		scrollPanel.setViewportView(content);

		messageText = new JTextPane();
		messageText.setSize(400, 50);
		messageText.setLocation(5, 415);
		messageText.setBorder(BorderFactory.createTitledBorder(""));
		messageText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		add(messageText);

		messageText.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				String message = messageText.getText();
				if (message.matches("[ |\n]+")) {
					messageText.setText("");
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == 10) {
					String message = messageText.getText();
					if (!message.matches("[ |\n]+") && !message.isEmpty()) {
						if (listener != null) {
							listener.onSend(name, message);
						}
						addMessage(name, message);
						messageText.setText("");
					}
				}
			}
		});

		for (int i = 0; i < 10; i++) {
			addMessage(
					"Cheick Mahady SISSOKO",
					"Ceci est un message de la part de Cheick\nSi tu n'es pas d'accord c'est ton probleme.");
		}

	}

	public void addMessage(String name, String message) {
		MessagePanel mp = new MessagePanel(name, message);
		mp.setLocation(lastX, lastY);
		lastX = mp.getX();
		lastY = mp.getY() + mp.getHeight();
		content.add(mp);
		content.setPreferredSize(new Dimension(content.getWidth(), lastY));
		repaint();
	}

	/**
	 * @param listener
	 *            the listener to set
	 */
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame f = new JFrame();
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				ChatBox cb = new ChatBox();
				for (int i = 0; i < 10; i++) {
					cb.addMessage(
							"Cheick Mahady SISSOKO",
							"Ceci est un message de la part de Cheick\nSi tu n'es pas d'accord c'est ton probleme.");
				}
				f.getContentPane().add(cb);
				f.setBounds(100, 100, 410, 600);
				f.setVisible(true);
			}
		});

	}

}
