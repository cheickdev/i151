/**
 * 
 */
package graphic.cards;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

/**
 * @author Sissoko
 * @date 26 avr. 2014 15:24:46
 */
public class ImageFactory {

	private static ImageFactory instance;

	public static URL getResource(String filename) {
		if (instance == null) {
			instance = new ImageFactory();
		}
		return instance.getClass().getResource(filename);
	}

	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null),
				img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();
		return bimage;
	}

	public static BufferedImage getBufferedImage(String filename) {
		return getBufferedImage(getResource(filename));
	}

	public static BufferedImage getBufferedImage(URL url) {
		if (url == null)
			return null;
		Image image;
		try {
			image = ImageIO.read(url);
			return toBufferedImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @param filename
	 * @return
	 */
	public static Image getImage(String filename) {
		return Toolkit.getDefaultToolkit().createImage(getResource(filename));
	}

	/**
	 * @param filename
	 * @return
	 */
	public static Image getImage(URL url) {
		return Toolkit.getDefaultToolkit().createImage(url);
	}

}
