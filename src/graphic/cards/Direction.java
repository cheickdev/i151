package graphic.cards;

import shared.Card;

public enum Direction {
	UP(0, 0), DOWN(Card.X - 50, Card.Y - 60);
	private final int top;
	private final int left;

	private Direction(int left, int top) {
		this.left = left;
		this.top = top;
	}

	public int left() {
		return left;
	}

	/**
	 * @return
	 */
	public int top() {
		return top;
	}
}