/**
 * 
 */
package graphic.cards;

import graphic.Orientation;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;

import shared.Card;
import shared.Deck;

/**
 * @author Sissoko
 * @date 26 avr. 2014 15:35:23
 */
public class CardPanelFactory {

	static int backPadding = 20;

	/**
	 * 
	 * @param cardPanels
	 * @return
	 */
	public static JLayeredPane createHandPanel(List<CardPanel> cardPanels) {
		JLayeredPane panel = new JLayeredPane();
		panel.setOpaque(false);
		panel.setSize(Card.X + cardPanels.size() * 32 - 15, Card.Y + 35);
		for (int i = cardPanels.size() - 1; i >= 0; i--) {
			int j = i - (cardPanels.size() / 2);
			CardPanel rcp = cardPanels.get(i);
			rcp.setLocation(15 + i * 32, 35);
			panel.add(rcp, new Integer(j));
		}
		return panel;
	}

	/**
	 * 
	 * @param n
	 * @return
	 */
	public static JLayeredPane createSidePanel(int n, Orientation orientation) {
		JLayeredPane panel = new JLayeredPane();
		panel.setOpaque(false);
		switch (orientation) {
		case HORIZONTAL:
			panel.setSize(Card.Y + 10, Card.X + n * backPadding);
			for (int i = n - 1; i >= 0; i--) {
				int j = i - (n / 2);
				BackCardPanel rcp = new BackCardPanel(orientation);
				rcp.setLocation(10, 15 + i * backPadding);
				panel.add(rcp, new Integer(j));
			}
			break;
		case VERTICAL:
			panel.setSize(Card.X + n * backPadding, Card.Y + 10);
			for (int i = n - 1; i >= 0; i--) {
				int j = i - (n / 2);
				BackCardPanel rcp = new BackCardPanel(orientation);
				rcp.setLocation(15 + i * backPadding, 10);
				panel.add(rcp, new Integer(j));
			}
			break;
		default:
			break;
		}
		// panel.setBorder(BorderFactory.createTitledBorder(""));
		return panel;
	}

	public static JLayeredPane createDeck(List<BackCardPanel> cardPanels) {
		JLayeredPane panel = new JLayeredPane();
		panel.setOpaque(false);
		panel.setSize(Card.X + cardPanels.size() * 2 + 10, Card.Y + 10);
		for (int i = cardPanels.size() - 1; i >= 0; i--) {
			int j = i - (cardPanels.size() / 2);
			BackCardPanel rcp = cardPanels.get(i);
			rcp.setLocation(15 + i * 2, 10);
			panel.add(rcp, new Integer(j));
		}
		return panel;
	}

	public static void main(String[] args) {
		final JFrame f = new JFrame();
		f.setLayout(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setBounds(100, 200, 2 * Card.X, Card.Y + 125);
		final Deck deck = new Deck();
		deck.riffleShuffle(7);
		// deck.poll(20);

		final LinkedList<CardPanel> panels = new LinkedList<CardPanel>();
		for (int i = 0; i < deck.size(); i++) {
			final CardPanel cp = new CardPanel(deck.get(i));
			cp.setListener(new CardPanel.Listener() {
				boolean b = true;

				@Override
				public void onClick(Card card, boolean control) {
					if (b) {
						cp.setLocation(cp.getX(), cp.getY() - 25);
					} else {
						cp.setLocation(cp.getX(), cp.getY() + 25);
					}
					b = !b;
					System.out.println(card);
					// cp.getParent().repaint();
				}
			});
			panels.add(cp);
		}
		int n = 5;
		JLayeredPane panePrincipal1 = CardPanelFactory.createHandPanel(panels);
		// JLayeredPane panePrincipal2 = CardPanelFactory.createDeck(panels);
		// panePrincipal2.setLocation(panePrincipal2.getX(),
		// panePrincipal2.getY()
		// + Card.Y);
		f.add(panePrincipal1);
		// f.add(panePrincipal2);
		f.setSize(panePrincipal1.getSize());
		// f.repaint();
		f.setVisible(true);
	}

}
