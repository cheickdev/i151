/**
 * 
 */
package graphic.cards;

import graphic.Orientation;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * @author Sissoko
 * @date 28 avr. 2014 23:25:38
 */
@SuppressWarnings("serial")
public class TopHandPanel extends JPanel {
	JLayeredPane root;

	/**
	 * 
	 */
	public TopHandPanel() {
		setLayout(null);
		setOpaque(false);
		root = new JLayeredPane();
		add(root);
	}

	/**
	 * 
	 * @param nbCards
	 */
	public void setCards(int nbCards) {
		remove(root);
		root = CardPanelFactory.createSidePanel(nbCards, Orientation.VERTICAL);
		setSize(root.getSize());
		add(root);
		repaint();
	}
}
