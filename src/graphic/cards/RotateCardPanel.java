/**
 * 
 */
package graphic.cards;

import java.awt.Graphics;
import java.awt.Graphics2D;

import shared.Card;

/**
 * @author Sissoko
 * @date 5 nov. 2013 00:44:17
 */
@SuppressWarnings("serial")
public class RotateCardPanel extends CardPanel {
	int n;

	/**
	 * 
	 */
	public RotateCardPanel(Card card) {
		super(card);
	}

	public RotateCardPanel(Card card, Integer n) {
		super(card);
		this.n = n;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		int w2 = getWidth() / 2;
		int h2 = getHeight() / 2;
		g2d.rotate(n * Math.PI / 32, w2, h2);
	}
}
