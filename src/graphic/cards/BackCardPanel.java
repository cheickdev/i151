/**
 * 
 */
package graphic.cards;

import graphic.Orientation;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import shared.Card;

/**
 * @author Sissoko
 * @date 26 avr. 2014 21:21:18
 */
@SuppressWarnings("serial")
public class BackCardPanel extends JPanel {

	Image bg;
	Listener listener;

	interface Listener {
		void onClick();
	}

	/**
	 * 
	 */
	public BackCardPanel(Orientation orientation) {
		setLayout(null);
		switch (orientation) {
		case HORIZONTAL:
			bg = Toolkit.getDefaultToolkit().getImage(
					ImageFactory.getResource("cardBackSide.png"));
			setSize(Card.Y, Card.X);
			break;
		case VERTICAL:
			bg = Toolkit.getDefaultToolkit().getImage(
					ImageFactory.getResource("cardBack.png"));
			setSize(Card.X, Card.Y);
			break;

		default:
			break;
		}
		setOpaque(true);
		setBorder(BorderFactory.createTitledBorder(""));
	}

	/**
	 * @param card
	 */
	public BackCardPanel() {
		setLayout(null);
		bg = Toolkit.getDefaultToolkit().getImage(
				ImageFactory.getResource("cardBack.png"));
		setSize(Card.X, Card.Y);
		addMouseListener(new MouseAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listener != null) {
					listener.onClick();
				}
			}
		});
	}

	/**
	 * 
	 * @param listener
	 */
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
	}
}
