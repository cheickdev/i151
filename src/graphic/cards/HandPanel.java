/**
 * 
 */
package graphic.cards;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

import shared.Card;
import shared.Hand;

/**
 * @author Sissoko
 * @date 28 avr. 2014 23:35:08
 */
@SuppressWarnings("serial")
public class HandPanel extends JPanel {

	public interface Listener {
		void onClick(List<Card> cards);
	}

	List<CardPanel> hand;
	List<CardPanel> handTmp;
	JLayeredPane root;
	List<Card> selectedCards;
	Listener listener;

	/**
	 * 
	 */
	public HandPanel() {
		setLayout(null);
		setOpaque(false);
		root = new JLayeredPane();
		hand = new LinkedList<CardPanel>();
		selectedCards = new LinkedList<>();
		handTmp = new LinkedList<CardPanel>();
		add(root);
	}

	/**
	 * 
	 * @param cards
	 */
	public void addCard(List<Card> cards) {
		if (cards.isEmpty()) {
			return;
		}
		for (Card card : cards) {
			final CardPanel cp = new CardPanel(card);
			cp.setListener(new CardPanel.Listener() {

				@Override
				public void onClick(Card card, boolean control) {
					System.out.println(card + ", " + control);
					if (control) {
						if (selectedCards.contains(card)) {
							selectedCards.remove(card);
							handTmp.remove(cp);
							cp.setLocation(cp.getX(), cp.getY() + 20);
						} else {
							selectedCards.add(card);
							handTmp.add(cp);
							cp.setLocation(cp.getX(), cp.getY() - 20);
						}
					} else {
						if (selectedCards.isEmpty()) {
							selectedCards.add(card);
							handTmp.add(cp);
							if (listener != null) {
								listener.onClick(selectedCards);
								removeCard(handTmp);
								selectedCards.clear();
							}
						} else {
							if (listener != null) {
								listener.onClick(selectedCards);
								removeCard(handTmp);
								selectedCards.clear();
							}
						}
					}
				}
			});
			hand.add(cp);
		}
		remove(root);
		root = CardPanelFactory.createHandPanel(hand);
		setSize(root.getSize());
		add(root);
		repaint();
	}

	/**
	 * 
	 * @param cards
	 */
	public void removeCard(List<CardPanel> cards) {
		if (cards.isEmpty()) {
			return;
		}
		for (CardPanel card : cards) {
			hand.remove(card);
		}
		remove(root);
		root = CardPanelFactory.createHandPanel(hand);
		setSize(root.getSize());
		add(root);
		repaint();
	}

	/**
	 * 
	 * @param card
	 */
	public void addCard(Card card) {
		if (card == null)
			return;
		hand.add(new CardPanel(card));
		remove(root);
		root = CardPanelFactory.createHandPanel(hand);
		setSize(root.getSize());
		add(root);
		repaint();
	}

	/**
	 * 
	 * @param card
	 */
	public void removeCard(Card card) {
		if (card == null)
			return;
		hand.remove(new CardPanel(card));
		remove(root);
		root = CardPanelFactory.createHandPanel(hand);
		setSize(root.getSize());
		add(root);
		repaint();
	}

	/**
	 * @param listener
	 *            the listener to set
	 */
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	/**
	 * 
	 * @param hand
	 */
	public void updateHand(Hand hand) {
		this.hand.clear();
		List<Card> cars = new LinkedList<>();
		for (Card c : hand) {
			cars.add(c);
		}
		addCard(cars);
	}

}
