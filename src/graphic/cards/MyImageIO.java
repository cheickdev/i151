/**
 * 
 */
package graphic.cards;

import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.IndexColorModel;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.JOptionPane;

/**
 * @author sissoko
 * @date 17 déc. 2012 21:40:33
 */
public class MyImageIO {

	public final static String ENCODE_BMP = "BMP";

	public final static String ENCODE_JPEG = "JPEG";

	public final static String ENCODE_PNG = "PNG";

	/**
	 * Represents an image with 8-bit RGB color components packed into integer
	 * pixels. The image has a {@link DirectColorModel} without alpha. When data
	 * with non-opaque alpha is stored in an image of this type, the color data
	 * must be adjusted to a non-premultiplied form and the alpha discarded, as
	 * described in the {@link java.awt.AlphaComposite} documentation.
	 */
	public static final int TYPE_INT_RGB = 1;

	/**
	 * Represents an image with 8-bit RGBA color components packed into integer
	 * pixels. The image has a <code>DirectColorModel</code> with alpha. The
	 * color data in this image is considered not to be premultiplied with
	 * alpha. When this type is used as the <code>imageType</code> argument to a
	 * <code>BufferedImage</code> constructor, the created image is consistent
	 * with images created in the JDK1.1 and earlier releases.
	 */
	public static final int TYPE_INT_ARGB = 2;

	/**
	 * Represents an image with 8-bit RGBA color components packed into integer
	 * pixels. The image has a <code>DirectColorModel</code> with alpha. The
	 * color data in this image is considered to be premultiplied with alpha.
	 */
	public static final int TYPE_INT_ARGB_PRE = 3;

	/**
	 * Represents an image with 8-bit RGB color components, corresponding to a
	 * Windows- or Solaris- style BGR color model, with the colors Blue, Green,
	 * and Red packed into integer pixels. There is no alpha. The image has a
	 * {@link DirectColorModel}. When data with non-opaque alpha is stored in an
	 * image of this type, the color data must be adjusted to a
	 * non-premultiplied form and the alpha discarded, as described in the
	 * {@link java.awt.AlphaComposite} documentation.
	 */
	public static final int TYPE_INT_BGR = 4;

	/**
	 * Represents an image with 8-bit RGB color components, corresponding to a
	 * Windows-style BGR color model) with the colors Blue, Green, and Red
	 * stored in 3 bytes. There is no alpha. The image has a
	 * <code>ComponentColorModel</code>. When data with non-opaque alpha is
	 * stored in an image of this type, the color data must be adjusted to a
	 * non-premultiplied form and the alpha discarded, as described in the
	 * {@link java.awt.AlphaComposite} documentation.
	 */
	public static final int TYPE_3BYTE_BGR = 5;

	/**
	 * Represents an image with 8-bit RGBA color components with the colors
	 * Blue, Green, and Red stored in 3 bytes and 1 byte of alpha. The image has
	 * a <code>ComponentColorModel</code> with alpha. The color data in this
	 * image is considered not to be premultiplied with alpha. The byte data is
	 * interleaved in a single byte array in the order A, B, G, R from lower to
	 * higher byte addresses within each pixel.
	 */
	public static final int TYPE_4BYTE_ABGR = 6;

	/**
	 * Represents an image with 8-bit RGBA color components with the colors
	 * Blue, Green, and Red stored in 3 bytes and 1 byte of alpha. The image has
	 * a <code>ComponentColorModel</code> with alpha. The color data in this
	 * image is considered to be premultiplied with alpha. The byte data is
	 * interleaved in a single byte array in the order A, B, G, R from lower to
	 * higher byte addresses within each pixel.
	 */
	public static final int TYPE_4BYTE_ABGR_PRE = 7;

	/**
	 * Represents an image with 5-6-5 RGB color components (5-bits red, 6-bits
	 * green, 5-bits blue) with no alpha. This image has a
	 * <code>DirectColorModel</code>. When data with non-opaque alpha is stored
	 * in an image of this type, the color data must be adjusted to a
	 * non-premultiplied form and the alpha discarded, as described in the
	 * {@link java.awt.AlphaComposite} documentation.
	 */
	public static final int TYPE_USHORT_565_RGB = 8;

	/**
	 * Represents an image with 5-5-5 RGB color components (5-bits red, 5-bits
	 * green, 5-bits blue) with no alpha. This image has a
	 * <code>DirectColorModel</code>. When data with non-opaque alpha is stored
	 * in an image of this type, the color data must be adjusted to a
	 * non-premultiplied form and the alpha discarded, as described in the
	 * {@link java.awt.AlphaComposite} documentation.
	 */
	public static final int TYPE_USHORT_555_RGB = 9;

	/**
	 * Represents a unsigned byte grayscale image, non-indexed. This image has a
	 * <code>ComponentColorModel</code> with a CS_GRAY {@link ColorSpace}. When
	 * data with non-opaque alpha is stored in an image of this type, the color
	 * data must be adjusted to a non-premultiplied form and the alpha
	 * discarded, as described in the {@link java.awt.AlphaComposite}
	 * documentation.
	 */
	public static final int TYPE_BYTE_GRAY = 10;

	/**
	 * Represents an unsigned short grayscale image, non-indexed). This image
	 * has a <code>ComponentColorModel</code> with a CS_GRAY
	 * <code>ColorSpace</code>. When data with non-opaque alpha is stored in an
	 * image of this type, the color data must be adjusted to a
	 * non-premultiplied form and the alpha discarded, as described in the
	 * {@link java.awt.AlphaComposite} documentation.
	 */
	public static final int TYPE_USHORT_GRAY = 11;

	/**
	 * Represents an opaque byte-packed 1, 2, or 4 bit image. The image has an
	 * {@link IndexColorModel} without alpha. When this type is used as the
	 * <code>imageType</code> argument to the <code>BufferedImage</code>
	 * constructor that takes an <code>imageType</code> argument but no
	 * <code>ColorModel</code> argument, a 1-bit image is created with an
	 * <code>IndexColorModel</code> with two colors in the default sRGB
	 * <code>ColorSpace</code>: {0,&nbsp;0,&nbsp;0} and
	 * {255,&nbsp;255,&nbsp;255}.
	 * 
	 * <p>
	 * Images with 2 or 4 bits per pixel may be constructed via the
	 * <code>BufferedImage</code> constructor that takes a
	 * <code>ColorModel</code> argument by supplying a <code>ColorModel</code>
	 * with an appropriate map size.
	 * 
	 * <p>
	 * Images with 8 bits per pixel should use the image types
	 * <code>TYPE_BYTE_INDEXED</code> or <code>TYPE_BYTE_GRAY</code> depending
	 * on their <code>ColorModel</code>.
	 * 
	 * <p>
	 * When color data is stored in an image of this type, the closest color in
	 * the colormap is determined by the <code>IndexColorModel</code> and the
	 * resulting index is stored. Approximation and loss of alpha or color
	 * components can result, depending on the colors in the
	 * <code>IndexColorModel</code> colormap.
	 */
	public static final int TYPE_BYTE_BINARY = 12;

	/**
	 * 
	 * @param obj
	 *            must be a array which content @param obj[0] = output file @param
	 *            obj[1] = image to save.
	 * @param imageType
	 * @param encode
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	public static void give(Object obj, int imageType, String encode)
			throws Exception {
		Object[] data = (Object[]) obj;
		if (data.length != 2)
			throw new Exception("Wrong data number: " + data.length);

		File file = (File) data[0];
		final RenderedImage image = toRenderedImage(data[1], imageType);

		OutputStream os = new FileOutputStream(file);

//		if (encode.equals(ENCODE_BMP)) {
//			BMPEncodeParam param = new BMPEncodeParam();
//			ImageEncoder enc = ImageCodec.createImageEncoder(ENCODE_BMP, os,
//					param);
//			enc.encode(image);
//			os.close();
//		} else if (encode.equals(ENCODE_JPEG)) {
//			JPEGEncodeParam param1 = new JPEGEncodeParam();
//			ImageEncoder enc1 = ImageCodec.createImageEncoder(ENCODE_JPEG, os,
//					param1);
//			enc1.encode(image);
//			os.close();
//		} else if (encode.equals(ENCODE_JPEG)) {
//			PNGEncodeParam param2 = PNGEncodeParam.getDefaultEncodeParam(image);
//			ImageEncoder enc2 = ImageCodec.createImageEncoder(ENCODE_PNG, os,
//					param2);
//			enc2.encode(image);
//			os.close();
//		} else {
//			throw new IllegalArgumentException("Unknown image type "
//					+ imageType);
//		}

	}

	private static RenderedImage toRenderedImage(Object obj, int imageType)
			throws Exception {
		if (obj instanceof RenderedImage) {
			RenderedImage image = (RenderedImage) obj;
			return toBufferedImage(image, imageType);
		}
		if (obj instanceof Image) {
			return toBufferedImage((Image) obj, imageType);
		}
		throw new Exception("Invalid data type: " + obj.getClass().getName());
	}

	private static BufferedImage toBufferedImage(Image image, int imageType) {
		BufferedImage buffImg = new BufferedImage(image.getWidth(null),
				image.getHeight(null), imageType);
		buffImg.createGraphics().drawImage(image, 0, 0, null);
		return buffImg;
	}

	private static BufferedImage toBufferedImage(RenderedImage image,
			int imageType) {
		BufferedImage buffImg = new BufferedImage(image.getWidth(),
				image.getHeight(), imageType);
		buffImg.createGraphics().drawRenderedImage(image, null);
		return buffImg;
	}

	public static void save(boolean[][] img, String filename) {
		long start = System.currentTimeMillis();
		try {
			PrintWriter pw = new PrintWriter(new FileOutputStream(new File(
					filename)));
			pw.println(img.length);
			if (img.length != 0)
				pw.println(img[0].length);
			for (int i = 0; i < img.length; i++) {
				for (int j = 0; j < img[i].length; j++) {
					pw.print((img[i][j]) ? "1" : "0");
				}
				pw.println();
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("Calcul fini en "
				+ ((System.currentTimeMillis() - start) / 1000) + "s");
	}

	public static void write(boolean[][] data, String file) {
		if (data == null || data.length == 0)
			return;
		if (file == null)
			file = JOptionPane
					.showInputDialog("Le nom du fichier de la matrice");
		try {
			PrintWriter pw = new PrintWriter(new File(file));
			pw.println(data.length + "\n" + data[0].length);
			for (int i = 0; i < data.length; i++) {
				int count = 0;
				for (int j = 0; j < data[i].length; j++) {
					pw.print((data[i][j] ? 1 : 0));
					for (int k = j + 1; k < data[i].length
							&& (data[i][j] == data[i][k]); k++) {
						count++;
					}
					if ((j + count) < (data[i].length - 1))
						pw.print(" " + (count + 1) + " ");
					else
						pw.print(" " + (count + 1));
					j = j + count;
					count = 0;
				}
				pw.println();
			}
			pw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static boolean[][] read(String filename) {
		boolean mat[][] = null;
		System.out.println("Scan en cours...");
		Scanner sc = new Scanner(System.in);
		try {
			sc = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String xx = sc.nextLine();
		if (!xx.matches("[0-9]+"))
			throw new Error("Format incorrect");
		int width = Integer.parseInt(xx);
		String yy = sc.nextLine();
		if (!yy.matches("[0-9]+"))
			throw new Error("Format incorrect");
		int height = Integer.parseInt(yy);
		mat = new boolean[width][height];
		for (int i = 0; i < width; i++) {
			String line = sc.nextLine();
			String tline[] = line.split(" ");
			int k = 0;
			for (int j = 0; 2 * j + 1 < tline.length && k < height; j++) {
				String bit = tline[2 * j];
				int occ = Integer.parseInt(tline[2 * j + 1]);
				for (int l = 0; l < occ; l++) {
					mat[i][k++] = bit.equals("1");
				}
			}
		}
		return mat;
	}

	public static boolean[][] matFromFile(String filename) {
		boolean mat[][] = null;
		System.out.println("Scan en cours...");
		Scanner sc = new Scanner(System.in);
		try {
			sc = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String xx = sc.nextLine();
		if (!xx.matches("[0-9]+"))
			throw new Error("Format incorrect");
		int width = Integer.parseInt(xx);
		String yy = sc.nextLine();
		if (!yy.matches("[0-9]+"))
			throw new Error("Format incorrect");
		int height = Integer.parseInt(yy);
		mat = new boolean[width][height];
		for (int i = 0; i < width; i++) {
			String line = sc.nextLine();
			for (int j = 0; j < line.length(); j++) {
				mat[i][j] = line.charAt(j) == '1';
			}
		}
		System.out.println("Scan terminé!");
		return mat;
	}
}
