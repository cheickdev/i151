/**
 * 
 */
package graphic.cards;

/**
 * @author Sissoko
 * @date 4 nov. 2013 19:48:52
 */
@SuppressWarnings("serial")
public class CoeurPanel extends CardPanelModel {

	/**
	 * @param value
	 * @param type
	 */
	public CoeurPanel(String value, Direction direction) {
		super(value, direction, CardPanelType.COEUR);
	}

}
