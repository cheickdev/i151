/**
 * 
 */
package graphic.cards;

import graphic.ColorPanel;
import graphic.Orientation;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import shared.Card;
import shared.Deck;

/**
 * @author sissoko
 * @date 22 déc. 2012 20:16:07
 */
@SuppressWarnings("serial")
public class CardPanel extends JPanel {
	public static double alpha = (Math.PI / 16.0f);
	Card card;
	Image bg;
	CardPanelModel cpu;
	CardPanelModel cpd;
	ColorPanel cp;

	public interface Listener {
		void onClick(Card card, boolean control);
	}

	Listener listener;

	/**
 * 
 */
	public CardPanel() {
		this(null);
	}

	/**
	 * 
	 */
	public CardPanel(Card card) {
		this.card = card;
		setLayout(null);
		bg = Toolkit.getDefaultToolkit().getImage(
				ImageFactory.getResource("bg2.jpg"));
		setLayout(null);
		setSize(Card.X, Card.Y);
		setLocation(30, 20);
		if (card != null) {
			setToolTipText(card.getTitle());
			CardPanelType type = CardPanelType.valueOf(card.getColor()
					.toUpperCase());
			cpu = type.makePanel(card.getNumber(), Direction.UP);
			cpd = type.makePanel(card.getNumber(), Direction.DOWN);
			cp = new ColorPanel(type);
			cp.setLocation((getWidth() - cp.getWidth()) / 2,
					(getHeight() - cp.getHeight()) / 2);
			add(cpu);
			add(cp);
			add(cpd);
			setOpaque(true);
			setBorder(BorderFactory.createTitledBorder(""));
			bind();
		}
	}

	/**
	 * 
	 * @param listener
	 */
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	private void bind() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listener != null) {
					listener.onClick(card, e.isControlDown());
				}
				// setLocation(getX() + 20, getY());
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(bg, 0, 0, getWidth(), getHeight(), this);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				final JFrame f = new JFrame();
				f.setLayout(null);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setBounds(100, 200, 2 * Card.X, Card.Y + 100);
				final Deck deck = new Deck();
				// deck.riffleShuffle(7);
				// deck.poll(20);

				final LinkedList<BackCardPanel> panels = new LinkedList<BackCardPanel>();
				for (int i = 0; i < deck.size(); i++) {
					final BackCardPanel cp = new BackCardPanel();
					cp.setListener(new BackCardPanel.Listener() {

						@Override
						public void onClick() {
							Container parent = cp.getParent();
							if (!deck.isEmpty() && parent != null) {
								parent.remove(cp);
								parent.repaint();
								System.out.println(deck.poll());
							}
						}
					});
					panels.add(cp);
				}
				int n = 5;
				JLayeredPane panePrincipal1 = CardPanelFactory.createSidePanel(
						n, Orientation.VERTICAL);
				JLayeredPane panePrincipal2 = CardPanelFactory
						.createDeck(panels);
				panePrincipal2.setLocation(panePrincipal2.getX(),
						panePrincipal2.getY() + Card.Y);
				f.add(panePrincipal1);
				f.add(panePrincipal2);
				f.setSize(400, 400);
				// f.repaint();
				f.setVisible(true);

			}
		});

	}

	public BufferedImage createBufferedImage() {
		int w = this.getWidth();
		int h = this.getHeight();
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		if (g == null) {
			return null;
		}
		this.paint(g);
		g.drawImage(bi, 0, 0, this);
		return bi;
	}

	public Image createImage() {
		int w = this.getWidth();
		int h = this.getHeight();
		Image image = this.createImage(w, h);
		if (image != null) {
			Graphics g = image.getGraphics();
			this.paint(g);
			g.drawImage(image, 0, 0, this);
		}
		return image;
	}

	/**
	 * @param card2
	 */
	public void setCard(Card card) {
		this.card = card;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((card == null) ? 0 : card.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CardPanel))
			return false;
		CardPanel other = (CardPanel) obj;
		if (card == null) {
			if (other.card != null)
				return false;
		} else if (!card.equals(other.card))
			return false;
		return true;
	}

}
