/**
 * 
 */
package graphic.cards;


/**
 * @author Sissoko
 * @date 4 nov. 2013 19:51:28
 */
@SuppressWarnings("serial")
public class CarreauPanel extends CardPanelModel {

	/**
	 * @param value
	 */
	public CarreauPanel(String value, Direction direction) {
		super(value, direction, CardPanelType.CARREAU);
	}

}
