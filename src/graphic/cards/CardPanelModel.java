/**
 * 
 */
package graphic.cards;

import graphic.ColorPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * @author Sissoko
 * @date 4 nov. 2013 19:33:21
 */
@SuppressWarnings("serial")
public abstract class CardPanelModel extends JPanel implements Cloneable {
	// public static final int WIDTH = 30, HEIGHT = 30;
	public static final Font font = new Font(Font.SANS_SERIF, Font.BOLD, 20);
	CardPanelType type;
	Direction direction;
	String value;
	Image image;
	Color color;
	JLabel valueLabel;

	public CardPanelModel(String value, Direction direction, CardPanelType type) {
		this.value = value;
		this.direction = direction;
		this.type = type;
		setSize(50, 60);
		image = Toolkit.getDefaultToolkit().getImage(
				getClass().getResource(type.name() + ".png"));
		setLocation(direction.left(), direction.top());
		switch (type) {
		case COEUR:
		case CARREAU:
			color = Color.red;
			break;
		default:
			color = Color.black;
			break;
		}
		// valueLabel.setBackground(Color.white);
		// add(valueLabel);
		setOpaque(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;
		switch (direction) {
		case UP:
			// remove(valueLabel);
			g.drawImage(image, 10, 25, 25, 25, this);
			if (value.length() == 1) {
				g.setFont(font);
				g.setColor(color);
				g.drawString(value, 15, 25);
			} else {
				g.setFont(font);
				g.setColor(color);
				g.drawString(value, 10, 25);
			}
			break;
		case DOWN:
			// remove(valueLabel);
			g.drawImage(image, 15, 10, 25, 25, this);
			int w2 = getWidth() / 2;
			int h2 = getHeight() / 2;
			g2d.rotate(Math.PI, w2, h2);
			if (value.length() == 1) {
				g.setFont(font);
				g.setColor(color);
				g.drawString(value, 15, 25);
			} else {
				g.setFont(font);
				g.setColor(color);
				g.drawString(value, 10, 25);
			}
			break;
		default:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected CardPanelModel clone() throws CloneNotSupportedException {
		CardPanelModel cp = (CardPanelModel) super.clone();
		return cp;
	}

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setLayout(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setBounds(100, 200, 150, 210);
		CardPanelModel cp = CardPanelType.PIQUE.makePanel("8", Direction.UP);
		cp.setBackground(Color.green);
		ColorPanel ccp = new ColorPanel(CardPanelType.PIQUE);
		ccp.setBackground(Color.red);
		ccp.setLocation((f.getWidth() - ccp.getWidth()) / 2,
				(f.getHeight() - ccp.getHeight()) / 2);
		CardPanelModel cpd = CardPanelType.PIQUE.makePanel("8", Direction.DOWN);
		cpd.setLocation(100, 130);
		cpd.setBackground(Color.blue);
		f.add(cp);
		f.add(ccp);
		f.add(cpd);
		f.setVisible(true);
	}
}
