/**
 * 
 */
package graphic.cards;

/**
 * @author Sissoko
 * @date 4 nov. 2013 19:28:11
 */
@SuppressWarnings("serial")
public class PiquePanel extends CardPanelModel {

	public PiquePanel(String value, Direction direction) {
		super(value, direction, CardPanelType.PIQUE);
	}

}
