/**
 * 
 */
package graphic.cards;

import graphic.Orientation;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * @author Sissoko
 * @date 29 avr. 2014 00:17:08
 */
@SuppressWarnings("serial")
public class BankCardPanel extends JPanel {

	JLayeredPane root;

	public interface Listener {
		void onClick();
	}

	Listener listener;

	/**
	 * 
	 */
	public BankCardPanel() {
		setLayout(null);
		setOpaque(false);
		root = new JLayeredPane();
		add(root);
	}

	/**
	 * 
	 * @param nbCards
	 */
	public void setCards(int nbCards) {
		List<BackCardPanel> cardPanels = new LinkedList<BackCardPanel>();
		if (nbCards == 0) {
			BackCardPanel bcp = new BackCardPanel(Orientation.VERTICAL);
			bcp.setListener(new BackCardPanel.Listener() {

				@Override
				public void onClick() {
					if (listener != null) {
						listener.onClick();
					}
				}
			});
			cardPanels.add(bcp);
		}
		for (int i = 0; i < nbCards; i++) {
			BackCardPanel bcp = new BackCardPanel(Orientation.VERTICAL);
			bcp.setListener(new BackCardPanel.Listener() {

				@Override
				public void onClick() {
					if (listener != null) {
						listener.onClick();
					}
				}
			});
			cardPanels.add(bcp);
		}
		remove(root);
		root = CardPanelFactory.createDeck(cardPanels);
		setSize(root.getSize());
		add(root);
		repaint();
	}

	/**
	 * @param listener
	 *            the listener to set
	 */
	public void setListener(Listener listener) {
		this.listener = listener;
	}
}
