/**
 * 
 */
package graphic.cards;


/**
 * @author Sissoko
 * @date 4 nov. 2013 19:50:35
 */
@SuppressWarnings("serial")
public class TreflePanel extends CardPanelModel {

	/**
	 * @param value
	 */
	public TreflePanel(String value, Direction direction) {
		super(value, direction, CardPanelType.TREFLE);
	}

}
