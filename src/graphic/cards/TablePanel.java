/**
 * 
 */
package graphic.cards;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.TexturePaint;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import shared.Card;
import shared.Deck;
import shared.TableHand;

/**
 * @author Sissoko
 * @date 26 avr. 2014 21:13:22
 */
@SuppressWarnings("serial")
public class TablePanel extends JPanel {

	Image table;
	TableHand playedCard;
	CardPanel topCard;
	JLayeredPane playedCardPanel;
	BufferedImage topCardImage;

	/**
	 * 
	 */
	public TablePanel() {
		setLayout(null);
		setOpaque(false);
		playedCard = new TableHand();
		playedCardPanel = new JLayeredPane();
		table = Toolkit.getDefaultToolkit().getImage(
				ImageFactory.getResource("tableFoot.png"));
		setSize(350, 380);
		playedCardPanel.setLocation(
				(getWidth() - playedCardPanel.getWidth()) / 2 - 15,
				(getHeight() - playedCardPanel.getHeight()) / 2 - 50);
		// timer.start();
	}

	Timer timer = new Timer(1000, new ActionListener() {
		int index = 0;
		final Deck deck = new Deck();

		@Override
		public void actionPerformed(ActionEvent e) {
			if (!deck.isEmpty()) {
				index = index % 32;
				addCard(deck.get(index));
				index++;
			}
			if (playedCard.isFull()) {
				playedCard.clear();
			}
		}
	});

	/**
	 * 
	 * @param cards
	 */
	public void addCards(List<Card> cards) {
		playedCard.push(cards);
		Card card = playedCard.getLast();
		remove(playedCardPanel);
		topCard = new CardPanel(card);
		List<CardPanel> cardPanels = new LinkedList<CardPanel>();
		int startIndex = playedCard.size() - 4;
		if (startIndex < 0)
			startIndex = 0;
		for (int i = startIndex; i < playedCard.size(); i++) {
			cardPanels.add(new CardPanel(playedCard.get(i)));
		}
		playedCardPanel = CardPanelFactory.createHandPanel(cardPanels);
		playedCardPanel.setLocation(
				(getWidth() - playedCardPanel.getWidth()) / 2 - 10,
				(getHeight() - playedCardPanel.getHeight()) / 2 - 50);
		add(playedCardPanel);
		repaint();
	}

	/**
	 * 
	 * @param card
	 */
	public void addCard(Card card) {
		playedCard.push(card);
		remove(playedCardPanel);
		topCard = new CardPanel(card);
		List<CardPanel> cardPanels = new LinkedList<CardPanel>();
		int startIndex = playedCard.size() - 4;
		if (startIndex < 0)
			startIndex = 0;
		for (int i = startIndex; i < playedCard.size(); i++) {
			cardPanels.add(new CardPanel(playedCard.get(i)));
		}
		playedCardPanel = CardPanelFactory.createHandPanel(cardPanels);
		playedCardPanel.setLocation(
				(getWidth() - playedCardPanel.getWidth()) / 2,
				(getHeight() - playedCardPanel.getHeight()) / 2 - 75);
		add(playedCardPanel);
		repaint();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		int width = 175;
		int height = 150;
		int tabx = (getWidth() - width) / 2;
		int taby = (getHeight() - height) / 2;
		g.drawImage(table, tabx, taby + height - 50, width, height, this);
		int ox = (getWidth() - (width + 150)) / 2;
		int oy = (getHeight() - (height + 100)) / 2 - 50;
		g.setColor(Color.white);
		BufferedImage tableBG = ImageFactory.getBufferedImage("paris.jpg");
		Polygon polygon = new Polygon(new int[] { ox, ox + (width + 150) * 2,
				ox + (width + 150) * 2, ox + (width + 150) * 2, ox },
				new int[] { oy, oy, oy + (height + 100) * 2,
						oy + (height + 100) * 2 }, 4);
		TexturePaint texture = new TexturePaint(tableBG, polygon.getBounds());
		g2d.setPaint(texture);
		g2d.fillOval(ox, oy, width + 150, height + 100);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				final JFrame f = new JFrame();
				f.setLayout(null);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setBounds(100, 200, 2 * Card.X, Card.Y + 100);
				final TablePanel pt = new TablePanel();
				f.add(pt);
				f.setSize(400, 400);
				f.setVisible(true);

			}
		});

	}

}
