/**
 * 
 */
package graphic.cards;

import graphic.Orientation;

import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 * @author Sissoko
 * @date 28 avr. 2014 23:31:34
 */
@SuppressWarnings("serial")
public class SideHandPanel extends JPanel {
	JLayeredPane root;

	/**
	 * 
	 */
	public SideHandPanel() {
		setLayout(null);
		setOpaque(false);
		root = new JLayeredPane();
		add(root);
	}

	/**
	 * 
	 * @param nbCards
	 */
	public void setCards(int nbCards) {
		remove(root);
		root = CardPanelFactory
				.createSidePanel(nbCards, Orientation.HORIZONTAL);
		setSize(root.getSize());
		add(root);
		repaint();
	}
}
