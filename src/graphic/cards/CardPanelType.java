/**
 * 
 */
package graphic.cards;

/**
 * @author Sissoko
 * @date 4 nov. 2013 19:32:51
 */
public enum CardPanelType {
	PIQUE {

		@Override
		public CardPanelModel makePanel(String value, Direction direction) {
			return new PiquePanel(value, direction);
		}

	},
	COEUR {

		@Override
		public CardPanelModel makePanel(String value, Direction direction) {
			return new CoeurPanel(value, direction);
		}

	},
	TREFLE {

		@Override
		public CardPanelModel makePanel(String value, Direction direction) {
			return new TreflePanel(value, direction);
		}

	},
	CARREAU {

		@Override
		public CardPanelModel makePanel(String value, Direction direction) {
			return new CarreauPanel(value, direction);
		}

	};

	abstract public CardPanelModel makePanel(String value, Direction dir);
}
