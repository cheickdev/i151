/**
 * 
 */
package graphic;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import shared.Card;
import shared.Deck;
import shared.Hand;

/**
 * @author sissoko
 * @date 21 févr. 2013 07:32:52
 */
public class HumanHand extends Hand {

	private LinkedList<Card> hands;

	public HumanHand() {
		hands = new LinkedList<Card>();
	}

	public void sort() {
		hands = mergeSort(hands);
	}

	@Override
	public Card get(int i) {
		return hands.get(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#poll(java.util.LinkedList)
	 */
	@Override
	public List<Card> poll(List<Card> cards) {
		hands.removeAll(cards);
		return cards;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#push(java.util.LinkedList)
	 */
	@Override
	public List<Card> push(List<Card> cards) {
		hands.addAll(cards);
		return cards;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#poll(shared.Card)
	 */
	@Override
	public Card poll(Card card) {
		hands.remove(card);
		return card;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#push(shared.Card)
	 */
	@Override
	public Card push(Card card) {
		hands.add(card);
		return card;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return hands.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#size()
	 */
	@Override
	public int size() {
		return hands.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#contains(shared.Card)
	 */
	@Override
	public boolean contains(Card card) {
		return hands.contains(card);
	}

	public String toString() {
		return hands.toString();
	}

	// Question 1.1. Tri par fusion pour une LinkedList
	private void split(LinkedList<Card> l, LinkedList<Card> l1,
			LinkedList<Card> l2) {
		int len = l.size();
		for (int i = 0; i < len; i++) {
			if (i % 2 == 0)
				l1.add(l.get(i));
			else
				l2.add(l.get(i));
		}
	}

	private LinkedList<Card> merge(LinkedList<Card> l1, LinkedList<Card> l2) {
		LinkedList<Card> l = new LinkedList<Card>();
		while (!l1.isEmpty() || !l2.isEmpty()) {
			if (!l1.isEmpty() && !l2.isEmpty()) {
				Card fl1 = l1.getFirst();
				Card fl2 = l2.getFirst();
				if (fl1.compareTo(fl2) <= 0) {
					l.add(l1.pollFirst());
				} else {
					l.add(l2.pollFirst());
				}
			} else {
				if (!l1.isEmpty()) {
					l.addAll(l1);
					return l;
				} else {
					l.addAll(l2);
					return l;
				}
			}
		}
		return l;
	}

	private LinkedList<Card> mergeSort(LinkedList<Card> l) {
		if (l.size() <= 1)
			return l;
		LinkedList<Card> l1 = new LinkedList<Card>();
		LinkedList<Card> l2 = new LinkedList<Card>();
		split(l, l1, l2);
		return merge(mergeSort(l1), mergeSort(l2));
	}

	public static void main(String[] args) {
		HumanHand hand = new HumanHand();
		Deck deck = new Deck();
		deck.riffleShuffle(8);
		for (int i = 0; i < deck.size(); i++) {
			hand.push(deck.get(i));
		}
		System.out.println(hand);
		hand.sort();
		System.out.println(hand);
	}

	@Override
	public void clear() {
		hands.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Card> iterator() {
		return hands.iterator();
	}

}
