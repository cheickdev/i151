/**
 * 
 */
package graphic;

import events.PlayEvent;
import events.PlayEventListener;
import graphic.cards.BankCardPanel;
import graphic.cards.HandPanel;
import graphic.cards.SideHandPanel;
import graphic.cards.TablePanel;
import graphic.cards.TopHandPanel;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import packets.Packet;
import packets.i151.BeginPacket;
import packets.i151.JoinGroupPacket;
import packets.i151.MessagePacket;
import packets.i151.TakePacket;
import shared.Card;
import shared.Deck;

/**
 * @author sissoko
 * @date 22 déc. 2012 20:14:29
 */
@SuppressWarnings("serial")
public class Window extends JFrame {

	// Table : (600, 165)
	// Hand : (594, 480)
	// Right : (945, 240)
	// Top : (675, 15)
	// Left : (425, 240)
	// Bank : (299, 15)
	// Chat : (15, 190)

	JPanel root;
	HandPanel handPanel;
	SideHandPanel rightHandPanel;
	TopHandPanel topHandPanel;
	SideHandPanel leftHandPanel;
	BankCardPanel bankCardPanel;
	TablePanel tablePanel;
	ChatBox chatBax;
	AskDialog askDialog;

	GraphicPlayer player;
	InterfaceClient client;
	protected String name = "Cheick";
	protected String managerId = "manager";
	Object monitor;

	/**
	 * 
	 */
	public Window() {
		setTitle("Jeu de cartes 151");
		setLayout(null);
		root = new WindowBackground();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(100, 50, 1120, 700);
		root.setSize(1120, 700);
		add(root);
		tablePanel = new TablePanel();
		int tpx = getWidth() - tablePanel.getWidth() - 170;
		int tpy = 165;
		tablePanel.setLocation(tpx, tpy);
		root.add(tablePanel);

		handPanel = new HandPanel();
		Deck deck = new Deck();
		handPanel.addCard(deck.poll(8));
		int hx = getWidth() - handPanel.getWidth() - 185;
		int hy = getHeight() - handPanel.getHeight() - 35;
		handPanel.setLocation(hx, hy);
		root.add(handPanel);

		rightHandPanel = new SideHandPanel();
		rightHandPanel.setCards(8);
		int rx = getWidth() - rightHandPanel.getWidth() - 15;
		int ry = getHeight() - rightHandPanel.getHeight()
				- handPanel.getHeight() - 15;
		rightHandPanel.setLocation(rx, ry);
		root.add(rightHandPanel);
		topHandPanel = new TopHandPanel();
		topHandPanel.setCards(8);
		int tx = getWidth() - topHandPanel.getWidth() - 185;
		int ty = 15;
		topHandPanel.setLocation(tx, ty);
		root.add(topHandPanel);

		leftHandPanel = new SideHandPanel();
		leftHandPanel.setCards(8);
		int lx = tablePanel.getX() - leftHandPanel.getWidth() - 15;
		int ly = getHeight() - rightHandPanel.getHeight()
				- handPanel.getHeight() - 15;
		leftHandPanel.setLocation(lx, ly);
		root.add(leftHandPanel);

		bankCardPanel = new BankCardPanel();
		bankCardPanel.setCards(8);

		int bx = tablePanel.getX() - leftHandPanel.getWidth()
				- bankCardPanel.getWidth() - 15;
		int by = 15;
		bankCardPanel.setLocation(bx, by);
		root.add(bankCardPanel);

		chatBax = new ChatBox(name);
		int cx = 15;
		int cy = bankCardPanel.getY() + bankCardPanel.getHeight() + 15;
		System.out.println("Chat : (" + cx + ", " + cy + ")");
		chatBax.setLocation(cx, cy);
		root.add(chatBax);

		player = new GraphicPlayer(name);
		askDialog = new AskDialog(this);
		bind();
		connect("localhost", 7000, "manager");
		player.send(new JoinGroupPacket(name, "server", 4, managerId));
	}

	private void bind() {

		chatBax.setListener(new ChatBox.Listener() {

			@Override
			public void onSend(String name, String message) {
				player.send(new MessagePacket(name, "server", message,
						"manager"));
			}
		});

		bankCardPanel.setListener(new BankCardPanel.Listener() {

			@Override
			public void onClick() {
				player.send(new TakePacket(name, "server", managerId));
			}
		});

		askDialog.setListener(new AskPanel.Listener() {

			@Override
			public void onClick(String color) {
				System.out.println(color);
				askDialog.dispose();
			}
		});

		handPanel.setListener(new HandPanel.Listener() {

			@Override
			public void onClick(List<Card> cards) {
				tablePanel.addCards(cards);
				player.poll(cards);
			}
		});
	}

	/**
	 * 
	 * @param host
	 * @param port
	 * @param managerId
	 */
	private void connect(String host, int port, String managerId) {
		boolean connect = player.connect(host, port, managerId);
		if (connect) {
			client = player.client;
			client.addPlayEventListener(new PlayEventListener() {

				@Override
				public void performedEvent(PlayEvent e) {
					manage(e.getPacket());
				}
			});

			new Thread(client).start();
		}
	}

	/**
	 * 
	 * @param packet
	 */
	private void handlePacket(Packet packet) {

		String topCard, cars;
		LinkedList<Card> cards;
		switch (packet.getType()) {
		case ASK:

			break;
		case ASKED:

			break;
		case CLOSED:

			break;
		case FAULT:
			System.out.println(packet);
			break;
		case GO_AROUND:

			break;
		case PLAY:
			topCard = packet.getContent();

			break;
		case MESSAGE:
			String name = packet.getSource();
			String message = packet.getContent();
			chatBax.addMessage(name, message);
			break;
		case PLAYED:
			System.out.println(packet);
			topCard = packet.getContent();
			cars = packet.getContent(1);
			if ("".equals(topCard)) {
			} else if (topCard.toLowerCase().matches(
					"pique||coeur||trefle||carreau")) {

			} else {
				cards = Card.decode(cars);

			}
			break;
		case SET_CARDS:
			cars = packet.getContent();
			cards = Card.decode(cars);
			player.put(cards);
			handPanel.updateHand(player.hand);
			int hx = getWidth() - handPanel.getWidth() - 185;
			int hy = getHeight() - handPanel.getHeight() - 35;
			handPanel.setLocation(hx, hy);
			break;
		case SET_MANAGER_ID:

			break;

		case SET_NAME:
			name = packet.getContent();
			break;
		case BANK_COUNT:
			int count = Integer.parseInt(packet.getContent());
			System.out.println(packet);
			break;
		case PLAYER_COUNT:

			break;
		case PLAYER_NAME:
			break;
		case TAKE:

			break;
		case TOOK:
			cars = packet.getContent();
			cards = Card.decode(cars);
			player.put(cards);
			handPanel.updateHand(player.hand);
			hx = getWidth() - handPanel.getWidth() - 185;
			hy = getHeight() - handPanel.getHeight() - 35;
			handPanel.setLocation(hx, hy);
			break;
		case CONNECTION:
			break;
		case JOIN_GROUP:
			break;
		case RAW:
			break;
		default:
			System.out.println(packet);
			break;
		}

	}

	private void manage(Packet packet) {
		String topCard, cars;
		LinkedList<Card> cards;
		int hx;
		int hy;
		switch (packet.getType()) {
		case AROUND:
			System.out.println(packet);
			break;
		case ASK_BEGIN:
			player.send(new BeginPacket(name, managerId, true, managerId));
			break;
		case ASKED:
			System.out.println(packet);
			break;
		case BANK_COUNT:
			int count = Integer.parseInt(packet.getContent());
			root.remove(bankCardPanel);
			bankCardPanel.setCards(count);
			int bx = tablePanel.getX() - leftHandPanel.getWidth()
					- bankCardPanel.getWidth() - 15;
			int by = 15;
			bankCardPanel.setLocation(bx, by);
			root.add(bankCardPanel);
			System.out.println(packet);
			root.repaint();
			break;
		case CLOSED:
			System.out.println(packet);
			break;
		case EXIST:
			System.out.println(packet);
			break;
		case FAULT:
			System.out.println(packet);
			break;
		case MESSAGE:
			String name = packet.getSource();
			String message = packet.getContent();
			chatBax.addMessage(name, message);
			break;
		case PLAY:
			topCard = packet.getContent();
			System.out.println(topCard);
			break;
		case PLAYED:
			System.out.println(packet);
			cards = Card.decode(packet.getContent());
			break;
		case PLAYER_COUNT:
			System.out.println(packet);
			break;
		case PLAYER_NAME:
			System.out.println(packet);
			break;
		case RAW:
			System.out.println(packet);
			break;
		case SET_CARDS:
			cars = packet.getContent();
			cards = Card.decode(cars);
			player.put(cards);
			handPanel.updateHand(player.hand);
			hx = getWidth() - handPanel.getWidth() - 185;
			hy = getHeight() - handPanel.getHeight() - 35;
			handPanel.setLocation(hx, hy);
			break;
		case SET_MANAGER_ID:
			System.out.println(packet);
			break;
		case SET_NAME:
			this.name = packet.getContent();
			setTitle(getTitle() + " " + this.name);
			System.out.println(packet);
			break;
		case TOOK:
			cars = packet.getContent();
			cards = Card.decode(cars);
			player.put(cards);
			handPanel.updateHand(player.hand);
			hx = getWidth() - handPanel.getWidth() - 185;
			hy = getHeight() - handPanel.getHeight() - 35;
			handPanel.setLocation(hx, hy);
			break;
		case WAITING:
			System.out.println(packet);
			break;
		case FULL:
			System.out.println(packet);
			break;
		default:
			break;
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new Window().setVisible(true);
			}
		});
	}

}
