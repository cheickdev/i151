/**
 * 
 */
package graphic;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;

import packets.Packet;
import events.PlayEvent;
import events.PlayEventContainer;
import events.PlayEventListener;

/**
 * @author Sissoko
 * @date 21 avr. 2014 15:54:30
 */
public class InterfaceClient extends PlayEventContainer implements
		PlayEventListener, Runnable {

	SocketChannel socket;

	/**
	 * @param socket
	 */
	public InterfaceClient(SocketChannel socket) {
		this.socket = socket;
	}

	/**
	 * @return
	 */
	public boolean isConnected() {
		return socket.isConnected();
	}

	/**
	 * @param encode
	 */
	public void write(ByteBuffer encode) {
		try {
			socket.write(encode);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * Send to the server.
	 * 
	 * @see events.PlayEventListener#performedEvent(events.PlayEvent)
	 */
	@Override
	public void performedEvent(PlayEvent e) {
		write(encode(e.getPacket()));
	}

	/**
	 * Converts data from the Java native {@code String} encoding to a
	 * conventional encoding for effective networking.
	 * 
	 * @param data
	 *            a {@code String} to be encoded
	 * @return a {@code ByteBuffer} containing the encoded data
	 */
	static ByteBuffer encode(String data) {
		return Packet.CONVERTER.encode(data);
	}

	/**
	 * Encodes a {@link packets#Packet Packet} to a conventional encoding for
	 * effective networking.
	 * 
	 * @param packet
	 *            a {@code Packet} to be encoded
	 * @return a {@code ByteBuffer} containing the encoded packet
	 */
	static ByteBuffer encode(Packet packet) {
		return encode(packet.getEncoding());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		String old[] = new String[] {};
		String last = "";
		while (true) {
			try {
				ByteBuffer bb = ByteBuffer.allocate(Packet.BUFFER_SIZE);
				int k = 0;
				k = socket.read(bb);
				bb.flip();
				if (k == 0)
					continue;
				if (k == -1) {
					socket.close();
					socket = null;
					System.err.println("Server goes away!");
					break;
				}
				CharBuffer chars = Packet.CONVERTER.decode(bb);
				String rawPacket = last + chars.toString();
				if (rawPacket.contains(Packet.PACKET_DELIMITER)) {
					String tab[] = rawPacket.split(Packet.PACKET_DELIMITER);
					old = Arrays.copyOfRange(tab, 0, tab.length - 1);
					for (String ps : old) {
						Packet packet = Packet.buildPacketFrom(ps);
						fireEvent(new PlayEvent(packet));
					}
					old = new String[] {};
					last = tab[tab.length - 1];
					if (rawPacket.endsWith(Packet.PACKET_DELIMITER)) {
						Packet packet = Packet.buildPacketFrom(last);
						fireEvent(new PlayEvent(packet));
						last = "";
					}
				} else if (rawPacket.endsWith(Packet.PACKET_DELIMITER)) {
					String tab[] = rawPacket.split(Packet.PACKET_DELIMITER);
					for (String ps : tab) {
						Packet packet = Packet.buildPacketFrom(ps);
						fireEvent(new PlayEvent(packet));
					}
					old = new String[] {};
					continue;
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
