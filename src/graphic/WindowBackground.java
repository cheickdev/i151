/**
 * 
 */
package graphic;

import graphic.cards.ImageFactory;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * @author Sissoko
 * @date 28 avr. 2014 20:35:28
 */
@SuppressWarnings("serial")
public class WindowBackground extends JPanel {

	Image backgroudImage;

	/**
	 * 
	 */
	public WindowBackground() {
		this("table_green.png");
	}

	public WindowBackground(String imageFile) {
		setLayout(null);
		backgroudImage = ImageFactory.getImage(imageFile);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(backgroudImage, 0, 0, this);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
