/**
 * 
 */
package graphic;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;

import javax.swing.JDialog;

/**
 * @author Sissoko
 * @date 4 mai 2014 00:47:42
 */
@SuppressWarnings("serial")
public class AskDialog extends JDialog {

	AskPanel panel;
	public boolean asking = false;

	/**
	 * @param owner
	 */
	public AskDialog(Frame owner) {
		super(owner);
		panel = new AskPanel();
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();

		setSize(310, panel.colorSize + 30);
		setLocation((screen.width - getSize().width) / 2,
				(screen.height - getSize().height) / 2);
		add(panel);
	}

	/**
	 * 
	 * @param listener
	 */
	public void setListener(AskPanel.Listener listener) {
		panel.setListener(listener);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final AskDialog ad = new AskDialog(null);
		ad.setListener(new AskPanel.Listener() {

			@Override
			public void onClick(String color) {
				System.out.println(color);
				ad.dispose();
			}
		});
		ad.show();
	}

}
