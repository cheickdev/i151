/**
 * 
 */
package graphic;

import graphic.cards.CardPanelType;
import graphic.cards.ImageFactory;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * @author Sissoko
 * @date 3 mai 2014 20:29:13
 */
@SuppressWarnings("serial")
public class AskPanel extends JPanel {

	public interface Listener {
		void onClick(String color);
	}

	Image backgroudImage;
	Listener listener;
	ColorPanel pique;
	ColorPanel coeur;
	ColorPanel trefle;
	ColorPanel carreau;

	final int colorSize = 75;

	/**
	 * 
	 */
	public AskPanel() {
		this("table_green.png");
	}

	/**
	 * 
	 */
	public AskPanel(String imageFile) {
		setLayout(null);
		backgroudImage = ImageFactory.getImage(imageFile);
		setSize(4 * colorSize + 10, colorSize + 10);
		pique = new ColorPanel(CardPanelType.PIQUE);
		pique.setSize(colorSize, colorSize);
		pique.setCursor(new Cursor(Cursor.HAND_CURSOR));
		pique.setBorder(BorderFactory.createTitledBorder(""));
		pique.setLocation(5, 5);
		add(pique);
		coeur = new ColorPanel(CardPanelType.COEUR);
		coeur.setSize(colorSize, colorSize);
		coeur.setCursor(new Cursor(Cursor.HAND_CURSOR));
		coeur.setBorder(BorderFactory.createTitledBorder(""));
		coeur.setLocation(5 + colorSize, 5);
		add(coeur);
		trefle = new ColorPanel(CardPanelType.TREFLE);
		trefle.setSize(colorSize, colorSize);
		trefle.setCursor(new Cursor(Cursor.HAND_CURSOR));
		trefle.setBorder(BorderFactory.createTitledBorder(""));
		trefle.setLocation(5 + 2 * colorSize, 5);
		add(trefle);
		carreau = new ColorPanel(CardPanelType.CARREAU);
		carreau.setSize(colorSize, colorSize);
		carreau.setCursor(new Cursor(Cursor.HAND_CURSOR));
		carreau.setBorder(BorderFactory.createTitledBorder(""));
		carreau.setLocation(5 + 3 * colorSize, 5);
		add(carreau);
		bind();
	}

	private void bind() {
		pique.addMouseListener(new MouseAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listener != null) {
					listener.onClick("pique");
				}
			}
		});

		coeur.addMouseListener(new MouseAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listener != null) {
					listener.onClick("coeur");
				}
			}
		});

		trefle.addMouseListener(new MouseAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listener != null) {
					listener.onClick("trefle");
				}
			}
		});

		carreau.addMouseListener(new MouseAdapter() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * java.awt.event.MouseAdapter#mouseClicked(java.awt.event.MouseEvent
			 * )
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				if (listener != null) {
					listener.onClick("carreau");
				}
			}
		});
	}

	/**
	 * @param listener
	 *            the listener to set
	 */
	public void setListener(Listener listener) {
		this.listener = listener;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(backgroudImage, 0, 0, this);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame f = new JFrame("Test");
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setBounds(100, 100, 310, 110);
				AskPanel ap = new AskPanel();
				ap.setListener(new Listener() {

					@Override
					public void onClick(String color) {
						System.out.println(color);
					}
				});
				f.add(ap);
				f.setVisible(true);
			}
		});
	}

}
