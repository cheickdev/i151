/**
 * 
 */
package graphic;

import graphic.cards.CardPanelType;
import graphic.cards.ImageFactory;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;

import shared.Card;

/**
 * @author Sissoko
 * @date 4 nov. 2013 23:02:14
 */
@SuppressWarnings("serial")
public class ColorPanel extends JPanel {
	CardPanelType type;
	Image image;

	/**
	 * 
	 */
	public ColorPanel() {
		setOpaque(false);
	}

	/**
	 * 
	 */
	public ColorPanel(CardPanelType type) {
		setLayout(null);
		this.type = type;
		image = Toolkit.getDefaultToolkit().getImage(
				ImageFactory.getResource(type.name() + ".png"));
		setSize(60, 60);
		setOpaque(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
	}

	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setBounds(100, 200, Card.X, Card.Y);
		ColorPanel cp = new ColorPanel(CardPanelType.CARREAU);
		f.add(cp);
		f.setVisible(true);
	}
}
