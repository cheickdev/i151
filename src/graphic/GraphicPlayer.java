/**
 * 
 */
package graphic;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

import packets.Packet;
import shared.Card;
import shared.Player;
import events.PlayEvent;

/**
 * @author sissoko
 * @date 22 déc. 2012 20:37:05
 */
public class GraphicPlayer extends Player {

	public GraphicPlayer(String name) {
		super(name);
		hand = new HumanHand();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#play(shared.Card)
	 */
	@Override
	public void play(Card top) {
		// TODO Auto-generated method stub
		// return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	@Override
	public void ask() {
		// TODO Auto-generated method stub
		// return null;
	}

	@Override
	public void play(String ask) {
		// TODO Auto-generated method stub
		// return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#connect(java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean connect(String host, int port, String managerId) {
		try {
			SocketChannel sc = SocketChannel.open();
			boolean b = sc.connect(new InetSocketAddress(host, port));
			if (b) {
				client = new InterfaceClient(sc);
			} else {
				System.out.println("Not connected");
			}
			return b;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#send(packets.Packet)
	 */
	@Override
	public void send(Packet packet) {
		if (packet == null)
			return;
		if (client != null) {
			try {
				// client.fireEvent(new GameEvent(packet));
				client.performedEvent(new PlayEvent(packet));
				// client.write(encode(packet));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#askBegin()
	 */
	@Override
	public boolean askBegin() {
		// TODO Auto-generated method stub
		return false;
	}

}
