/**
 * 
 */
package events;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Cheick Mahady Sissoko
 * @date 21 avr. 2014 14:40:51
 */
public class PlayEventContainer implements Iterable<PlayEventListener> {
	List<PlayEventListener> events;

	/**
	 * 
	 */
	public PlayEventContainer() {
		events = new ArrayList<>();
	}

	/**
	 * 
	 * @param listener
	 */
	public void addPlayEventListener(PlayEventListener listener) {
		events.add(listener);
	}

	/**
	 * 
	 * @param listener
	 * @return
	 */
	public boolean removePlayEventListener(PlayEventListener listener) {
		return events.remove(listener);
	}

	/**
	 * 
	 * @param event
	 */
	public void fireEvent(PlayEvent event) {
		for (PlayEventListener pel : events) {
			pel.performedEvent(event);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<PlayEventListener> iterator() {
		return events.iterator();
	}
}
