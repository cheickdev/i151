/**
 * 
 */
package events;

import packets.Packet;

/**
 * @author Cheick Mahady Sissoko
 * @date 21 avr. 2014 12:57:52
 */
public class PlayEvent {

	protected final Packet packet;

	/**
	 * @param packet2
	 */
	public PlayEvent(Packet packet) {
		this.packet = packet;
	}

	public Packet getPacket() {
		return packet;
	}

}
