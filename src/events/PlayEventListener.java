/**
 * 
 */
package events;

/**
 * @author Cheick Mahady Sissoko
 * @date 21 avr. 2014 14:17:25
 */
public interface PlayEventListener {
	/**
	 * 
	 * @param e
	 */
	void performedEvent(PlayEvent e);
}
