/**
 * 
 */
package shared;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Sissoko
 * @date 21 avr. 2014 18:20:23
 */
public class TableHand extends Hand {
	LinkedList<Card> hands;

	/**
	 * 
	 */
	public TableHand() {
		hands = new LinkedList<>();
	}

	public Card getLast() {
		if (hands.isEmpty())
			return null;
		return hands.getLast();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#poll(java.util.LinkedList)
	 */
	@Override
	public List<Card> poll(List<Card> cards) {
		boolean b = hands.removeAll(cards);
		if (!b)
			return null;
		return cards;
	}

	public boolean isFull() {
		return size() >= MAX_CARDS;
	}

	public boolean isFull(int i) {
		return size() + i > MAX_CARDS;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#push(java.util.LinkedList)
	 */
	@Override
	public List<Card> push(List<Card> cards) {
		if (!isFull(cards.size())) {
			hands.addAll(cards);
			return cards;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#poll(shared.Card)
	 */
	@Override
	public Card poll(Card card) {
		boolean b = hands.remove(card);
		if (b) {
			return card;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#push(shared.Card)
	 */
	@Override
	public Card push(Card card) {
		if (!isFull(1)) {
			hands.add(card);
			return card;
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return size() == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#size()
	 */
	@Override
	public int size() {
		return hands.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#contains(shared.Card)
	 */
	@Override
	public boolean contains(Card card) {
		return hands.contains(card);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#get(int)
	 */
	@Override
	public Card get(int i) {
		return hands.get(i);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#clear()
	 */
	@Override
	public void clear() {
		hands.clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Card> iterator() {
		// TODO Auto-generated method stub
		return hands.iterator();
	}

}
