/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package shared;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * 
 * @author Mcicheick 22/12/2011
 */
public class Deck {

	public static final int MAX_CARDS = 32;
	/**
	 * 
	 */
	private String[] couleurs = new String[] { "pique", "coeur", "trefle",
			"carreau" };
	private final LinkedList<Card> deck;

	private final LinkedList<Card> table;

	private Card topCard;

	public Deck() {
		deck = new LinkedList<Card>();
		table = new LinkedList<Card>();
		topCard = null;
		for (String num : Card.listeNumero) {
			for (String color : Card.listeCouleur) {
				deck.add(new Card(num, color));
			}
		}
	}

	public Deck(boolean flag) {
		deck = new LinkedList<Card>();
		table = new LinkedList<Card>();
		for (int i = 10; i > 1; i--) {
			for (int j = 0; j < 4; j++) {
				if (i > 6 && i != 8)
					deck.add(new Card(i + "", couleurs[j]));
				if (i == 6) {
					deck.add(new Card("R", couleurs[j]));
				} else if (i == 5) {
					deck.add(new Card("V", couleurs[j]));
				} else if (i == 4) {
					deck.add(new Card("D", couleurs[j]));
				} else if (i == 3) {
					deck.add(new Card("1", couleurs[j]));
				} else if (i == 2) {
					deck.add(new Card("8", couleurs[j]));
				}
			}
		}
	}

	public Deck(LinkedList<Card> cl) {
		deck = cl;
		table = new LinkedList<Card>();
	}

	public LinkedList<Card> getTable() {
		return table;
	}

	public Card getTopCard() {
		return topCard;
	}

	public Card poll() {
		Card card = deck.pollFirst();
		if (deck.isEmpty())
			reverseTable();
		if (deck.isEmpty())
			topCard = null;
		return card;
	}

	public LinkedList<Card> poll(int n) {
		LinkedList<Card> cards = new LinkedList<Card>();
		for (int i = 0; i < n; i++) {
			if (isEmpty())
				return cards;
			cards.add(poll());
		}
		return cards;
	}

	public Card getFirst() {
		return deck.peekFirst();
	}

	public Card getLast() {
		return deck.peekLast();
	}

	public Card get(int i) {
		return deck.get(i);
	}

	public int size() {
		return deck.size();
	}

	public boolean isEmpty() {
		if (!deck.isEmpty())
			return false;
		if (!table.isEmpty())
			return false;
		return true;
	}

	public void reverseTable() {
		if (!table.isEmpty())
			topCard = table.getLast();
		while (!table.isEmpty()) {
			deck.addFirst(table.pollFirst());
		}
	}

	public void putOnTable(Card card) {
		topCard = card;
		table.add(card);
	}

	public void putOnTable(LinkedList<Card> cards) {
		table.addAll(cards);
		topCard = table.peekLast();
	}

	private int cut(int n) {
		int res = 0;
		for (int i = 0; i < n; i++)
			if (Math.random() < 0.5)
				res++;
		return res;
	}

	private LinkedList<Card> split(int c) {
		LinkedList<Card> cards = new LinkedList<Card>();
		for (int i = 0; i < c; i++) {
			cards.add(poll());
		}
		return cards;
	}

	private LinkedList<Card> riffle(LinkedList<Card> l2) {
		double k1 = deck.size();
		double k2 = l2.size();
		double p1 = k1 / (k1 + k2);
		LinkedList<Card> cards = new LinkedList<Card>();
		for (; !(deck.isEmpty() && l2.isEmpty());) {
			k1 = deck.size();
			k2 = l2.size();
			p1 = k1 / (k1 + k2);
			if (Math.random() < p1)
				cards.add(poll());
			else
				cards.add(l2.remove(l2.size() - 1));
		}
		return cards;
	}

	public void riffleShuffle(int n) {
		for (int i = 0; i < n; i++) {
			deck.addAll(riffle(split(cut(deck.size()))));
		}
	}

	@Override
	public String toString() {
		return Arrays.toString(deck.toArray());
	}

	public void end() {
		topCard = null;
	}

	/**
	 * @param hand
	 */
	public void push(Hand hand) {
		for (Card c : hand) {
			deck.addLast(c);
		}
	}
}