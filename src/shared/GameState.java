/**
 * 
 */
package shared;

/**
 * @author Mcicheick 21/12/2011
 */
public class GameState {
	String demande;

	boolean finJeu;

	String personId;

	String deck;

	String tables;

	String carteHaute;

	/**
	 * 
	 */
	public GameState() {
		super();
	}

	/**
	 * @param rPlayers
	 * @param deck
	 * @param tables
	 */
	public GameState(String personId, String demande, boolean finJeu) {
		super();
		this.personId = personId;
		this.demande = demande;
		this.finJeu = finJeu;
	}

	/**
	 * @return the personId
	 */
	public String getPersonId() {
		return personId;
	}

	/**
	 * @param personId
	 *            the personId to set
	 */
	public void setPersonId(String personId) {
		this.personId = personId;
	}

	/**
	 * @return the deck
	 */
	public String getDeck() {
		return deck;
	}

	/**
	 * @param deck
	 *            the deck to set
	 */
	public void setDeck(String deck) {
		this.deck = deck;
	}

	/**
	 * @return the table
	 */
	public String getTables() {
		return tables;
	}

	/**
	 * @param table
	 *            the table to set
	 */
	public void setTables(String tables) {
		this.tables = tables;
	}

	/**
	 * @return the carteHaute
	 */
	public String getCarteHaute() {
		return carteHaute;
	}

	/**
	 * @param carteHaute
	 *            the carteHaute to set
	 */
	public void setCarteHaute(String carteHaute) {
		this.carteHaute = carteHaute;
	}

	/**
	 * @return the demande
	 */
	public String getDemande() {
		return demande;
	}

	/**
	 * @param demande
	 *            the demande to set
	 */
	public void setDemande(String demande) {
		this.demande = demande;
	}

	/**
	 * @return the finJeu
	 */
	public boolean isFinJeu() {
		return finJeu;
	}

	/**
	 * @param finJeu
	 *            the finJeu to set
	 */
	public void setFinJeu(boolean finJeu) {
		this.finJeu = finJeu;
	}
}
