/**
 * 
 */
package shared;

import events.PlayEvent;
import events.PlayEventListener;
import graphic.HumanHand;
import graphic.InterfaceClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.LinkedList;

import packets.Packet;
import packets.PacketType;
import packets.i151.AskedPacket;
import packets.i151.CardsPacket;
import packets.i151.GoAroundPacket;
import packets.i151.JoinGroupPacket;
import packets.i151.MessagePacket;
import packets.i151.PlayPacket;
import packets.i151.PlayedPacket;
import packets.i151.TakePacket;

/**
 * @author sissoko
 * @date 22 déc. 2012 20:39:11
 */
public class ConsolePlayer extends Player {

	private static final String REGEX_1 = "[0-9]{1,2}((,( +)?[0-9]{1,2})+)?";
	private static final String REGEX_2 = "-[0-9]+";
	private static final String[] cmds = new String[] { "", "PASSER", "PIOCHER" };
	Thread threadClient;
	protected InterfaceClient client;
	protected BufferedReader istream;
	protected PrintStream ostream;

	public ConsolePlayer(String name) {
		super(name);
		this.hand = new HumanHand();
		istream = new BufferedReader(new InputStreamReader(System.in));
		ostream = System.out;
		table = new TableHand();
	}

	private String readLine(String msg) throws IOException {
		System.out.print(msg);
		String line = istream.readLine();
		return line;
	}

	/**
	 * 
	 * @param line
	 * @return
	 * @throws Exception
	 */
	private LinkedList<Card> createListFromLine(String line) throws Exception {
		if (line.matches(REGEX_1)) {
			LinkedList<Card> cards = new LinkedList<Card>();
			String[] index = line.split(",( +)?");
			for (String ind : index) {
				int i = Integer.parseInt(ind);
				if (i >= hand.size())
					throw new Exception("Wrong argument " + i);
				Card card = hand.get(i);
				cards.add(card);
			}
			return cards;
		} else if (line.matches(REGEX_2)) {
			int i = Integer.parseInt(line);
			if (-i >= cmds.length)
				throw new Exception("Wrong argument " + i);
			i = -i;
			String action = cmds[i];
			System.out.println(action);
			return null;
		}
		throw new Exception("Wrong argument " + line);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#play(shared.Card)
	 */
	@Override
	public void play(Card top) {
		String[] cs;
		int u;
		try {
			printHand();
			String line = readLine(top + " : ");
			if (line.startsWith("ms")) {
				String message = line.substring(line.indexOf(":") + 1);
				send(new MessagePacket(name, "server", message, managerId));
				play(top);
				return;
			}
			cs = line.split(",");
			LinkedList<Card> cards = new LinkedList<>();
			for (int j = 0; j < cs.length; j++) {
				u = Integer.parseInt(cs[j]);
				if (u == 0) {
					control("go_around");
					return;
				}
				if (u == -1) {
					control("take");
					return;
				}
				u--;
				cards.add(hand.get(u));
			}
			if (!cards.isEmpty()) {
				send(new PlayedPacket(name, "server", Card.encode(cards),
						managerId));
			} else {
				send(new TakePacket(name, "server", managerId));
			}
			return;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			play(top);
			return;
		}
	}

	private void printHand() {
		ostream.print("\nex : ms pour message -1 pour piocher 0 pour passer sinon 1,2, 3");
		ostream.print("\nVotre main:");
		for (int j = 0; j < hand.size(); j++) {
			ostream.print(" " + (j + 1) + "=" + hand.get(j));
		}
		ostream.print("\n" + name + " : Quelles cartes voulez-vous jouees : ");
		ostream.print("\nVos cartes:");
		ostream.println();
	}

	@Override
	public void play(String ask) {
		try {
			String[] cs;
			int u;
			printHand();
			String line = readLine(ask + " : ");
			cs = line.split(",");
			LinkedList<Card> cards = new LinkedList<>();
			for (int j = 0; j < cs.length; j++) {
				u = Integer.parseInt(cs[j]);
				if (u == 0) {
					control("go_around");
					return;
				}
				if (u == -1) {
					control("take");
					return;
				}
				u--;
				cards.add(hand.get(u));
			}
			if (!cards.isEmpty()) {
				send(new PlayedPacket(name, "server", Card.encode(cards),
						managerId));
			} else {
				send(new TakePacket(name, "server", managerId));
			}
			return;
		} catch (Exception e) {
			System.err.println(e.getMessage());
			play(ask);
			return;
		}
	}

	@Override
	public void ask() {
		String asked;
		try {
			ostream.print("\nQuelle couleur voulez-vous demandee : ");
			ostream.print("\n");
			for (int i = 0; i < Card.listeCouleur.length; i++) {
				ostream.print(" " + (i + 1) + "=" + Card.listeCouleur[i]);
			}
			ostream.print("\nCouleur : ");
			asked = readLine("p = pique; c = coeur, t = trefle, k = carreau\nChoice : ");

			if (asked == null) {
				ask();
				return;
			}
			if (!asked.toLowerCase().matches(
					"(p||pique||c||coeur||t||trefle||k||carreau)")) {
				System.err.println("\nWrong ask try again");
				ask();
				return;
			}
			asked = asked.toUpperCase();
			CardColor color = CardColor.valueOf(asked);
			switch (color) {
			case PIQUE:
			case P:
				send(new AskedPacket(name, "server", "pique", managerId));
				break;
			case COEUR:
			case C:
				send(new AskedPacket(name, "server", "coeur", managerId));
				break;
			case TREFLE:
			case T:
				send(new AskedPacket(name, "server", "trefle", managerId));
				break;
			case CARREAU:
			case K:
				send(new AskedPacket(name, "server", "carreau", managerId));
				break;
			default:
				break;
			}
			if (asked.isEmpty()) {
				System.err.println("\nWrong ask try again");
				ask();
				return;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#receive(packets.Packet)
	 */
	@Override
	public void receive(Packet packet) {
		String topCard, cars;
		LinkedList<Card> cards;
		switch (packet.getType()) {
		case ASK:
			// if (packet.getDestination().equals(name)) {
			// ask(null);
			// }
			ask();
			break;
		case ASKED:
			ask = packet.getContent();
			System.out.println("Asked : " + ask);
			break;
		case CLOSED:
			System.out.println("Closed : " + packet);
			threadClient.stop();
			break;
		case FAULT:
			System.out.println(packet);
			break;
		case GO_AROUND:
			send(new GoAroundPacket(name, "server", managerId));
			System.out.println(packet);
			break;
		case PLAY:
			topCard = packet.getContent();
			if ("".equals(topCard)) {
				play((Card) null);
			} else if (topCard.toLowerCase().matches(
					"pique||coeur||trefle||carreau")) {
				play(topCard);
			} else {
				Card card = Card.createCard(topCard);
				play(card);
			}
			break;
		case MESSAGE:
			ostream.println("Message from : " + packet.getSource() + " : "
					+ packet.getContent());
			break;
		case PLAYED:
			System.out.println(packet);
			topCard = packet.getContent();
			cars = packet.getContent(1);
			if ("".equals(topCard)) {
			} else if (topCard.toLowerCase().matches(
					"pique||coeur||trefle||carreau")) {

			} else {
				cards = Card.decode(cars);
				// if (packet.getSource().equals(name)) {
				//
				// }
				hand.poll(cards);
				table.push(cards);
			}
			break;
		case SET_CARDS:
			cars = packet.getContent();
			cards = Card.decode(cars);
			hand.push(cards);
			ostream.println(hand);
			// if (packet.getDestination().equals(name)) {
			// hand.push(cards);
			// ostream.println(hand);
			// }
			break;
		case SET_MANAGER_ID:
			this.managerId = packet.getContent();
			ostream.println("managerId set to " + managerId);
			break;

		case SET_NAME:
			name = packet.getContent();
			ostream.println("name set to " + name);
			break;
		case TAKE:
			send(new TakePacket(name, "server", managerId));
			ostream.println(packet);
			break;
		case TOOK:
			cars = packet.getContent();
			cards = Card.decode(cars);
			if (packet.getSource().equals(name)) {
				hand.push(cards);
				ostream.println(hand);
			}
			break;
		default:
			System.out.println(packet);
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#connect(java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean connect(String host, int port, String managerId) {
		try {
			SocketChannel sc = SocketChannel.open();
			boolean b = sc.connect(new InetSocketAddress(host, port));
			if (b) {
				client = new InterfaceClient(sc);
				client.addPlayEventListener(new PlayEventListener() {

					@Override
					public void performedEvent(PlayEvent e) {
						receive(e.getPacket());
					}
				});
				threadClient = new Thread(client);
				threadClient.start();
			} else {
				System.out.println("Not connected");
			}
			return b;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private void control(String cmd) {
		System.out.println(cmd);
		if (cmd == null)
			return;
		String[] tab = cmd.split(" ", 2);
		try {
			PacketType type = PacketType.valueOf(tab[0].toUpperCase());
			if (type != null) {
				switch (type) {
				case ASK:
					ask();
					break;
				case ASKED:

					send(new AskedPacket(name, "server", tab[1], managerId));
					break;
				case JOIN_GROUP:
					managerId = tab[1];
					send(new JoinGroupPacket(name, "server", 2, tab[1]));
					break;
				case CONNECTION:
					String tt[] = tab[1].split(" ");
					connect(tt[0], Integer.parseInt(tt[1]), managerId);
					break;
				case FAULT:

					break;
				case GO_AROUND:
					send(new GoAroundPacket(name, "server", managerId));
					break;
				case MESSAGE:
					if (cmd.length() > 1)
						send(new MessagePacket(name, "server", tab[1],
								managerId));
					break;
				case PLAYED:
					send(new PlayedPacket(name, "server",
							"[1 pique]; [1 trefle]; [K, pique]", managerId));
					break;
				case PLAY:
					send(new PlayPacket(name, "server", "[1 pique]", managerId));
					break;
				case TAKE:
					send(new TakePacket(name, "server", managerId));
					break;
				case TOOK:
					// send(new TookPacket(name, "server",
					// "[1 pique]; [1 trefle]"));
					break;
				case SET_CARDS:
					send(new CardsPacket(name, "server", "[K,pique]", managerId));
					break;
				default:
					if (cmd.equalsIgnoreCase("exit")) {
						System.out.println("Goodbye!");
						System.exit(0);
					} else {
						System.out.println("Commande non reconnue : " + tab[0]);
					}
					break;
				}
			}
		} catch (Exception e) {
			if (cmd.equalsIgnoreCase("exit")) {
				System.out.println("Goodbye!");
				System.exit(0);
			} else {
				System.err.println(e);
				System.out.println("Commande non reconnue : " + tab[0]);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#send(packets.Packet)
	 */
	@Override
	public void send(Packet packet) {
		if (packet == null)
			return;
		if (client != null) {
			try {
				// client.fireEvent(new GameEvent(packet));
				client.performedEvent(new PlayEvent(packet));
				// client.write(encode(packet));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ConsolePlayer cp = new ConsolePlayer("Mac");
		cp.control("connection localhost 7000");
		System.out.println(cp.name + " running");
		String cmd;
		try {
			cmd = cp.readLine("Command : ");
			while (cmd != null) {
				cp.control(cmd);
				if (cmd.startsWith("join")) {
					break;
				}
				cmd = cp.readLine("Command : ");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#askBegin()
	 */
	@Override
	public boolean askBegin() {
		// TODO Auto-generated method stub
		return false;
	}

}
