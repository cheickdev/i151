/**
 * 
 */
package shared;

import java.util.List;

/**
 * @author sissoko
 * @date 20 févr. 2013 19:11:23
 */
public abstract class Hand implements Iterable<Card> {
	// public static int SIZE = 8;
	public static final int MAX_CARDS = 32;

	/**
	 * 
	 * @param cards
	 * @return
	 */
	public abstract List<Card> poll(List<Card> cards);

	/**
	 * 
	 * @param cards
	 * @return
	 */
	public abstract List<Card> push(List<Card> cards);

	/**
	 * 
	 * @param card
	 * @return
	 */
	public abstract Card poll(Card card);

	/**
	 * 
	 * @param card
	 * @return
	 */
	public abstract Card push(Card card);

	/**
	 * 
	 * @return
	 */
	public abstract boolean isEmpty();

	/**
	 * 
	 * @return
	 */
	public abstract int size();

	/**
	 * 
	 * @param card
	 * @return
	 */
	public abstract boolean contains(Card card);

	/**
	 * 
	 * @param i
	 * @return
	 */
	public abstract Card get(int i);

	/**
	 * 
	 */
	public abstract void clear();

}
