/**
 * 
 */
package shared;

import java.util.LinkedList;
import java.util.List;

/**
 * @author sissoko
 * @date 22 déc. 2012 19:40:33
 */
public class Card implements Comparable<Card> {

	// Card fields
	private String number, color;
	private int poids = 0;

	public static final int X = 100, Y = 150;

	/**
	 * Create card with his number and his color
	 * 
	 * @param number
	 * @param color
	 */
	public Card(String number, String color) {
		this.number = number;
		this.color = color;
		if (number.equals("10"))
			poids = 0;
		else if (number.equals("9"))
			poids = 1;
		else if (number.equals("7"))
			poids = 2;
		else if (number.equals("R") || number.equals("K"))
			poids = 3;
		else if (number.equals("V") || number.equals("J"))
			poids = 4;
		else if (number.equals("D") || number.equals("Q"))
			poids = 5;
		else if (number.equals("1") || number.equals("A"))
			poids = 6;
		else if (number.equals("8"))
			poids = 7;
	}

	/**
	 * 
	 * @return color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * 
	 * 
	 * @return number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * 
	 * @return card value
	 */
	public int getValeur() {
		if (number.equals("1") || number.equals("A"))
			return 11;
		else if (number.equals("V") || number.equals("J"))
			return 2;
		else if (number.equals("D") || number.equals("Q"))
			return 3;
		else if (number.equals("R") || number.equals("K"))
			return 4;
		else if (number.equals("8"))
			return 32;
		else
			return Integer.parseInt(number);
	}

	public int getPoids() {
		return poids;
	}

	/**
	 * set card color
	 * 
	 * @param color
	 */
	public void setCouleur(String color) {
		this.color = color;
	}

	/**
	 * set card number
	 * 
	 * @param number
	 */
	public void setNumero(String number) {
		this.number = number;
	}

	/**
	 * 
	 * @param c
	 * @return return true if this is equivalent with @param c
	 */
	public boolean isEquivalent(Card c) {
		if (c != null && c.is8())
			return true;
		if (isAs())
			return c != null && c.isAs();
		return (sameColor(c) || sameNumber(c));
	}

	/**
	 * Verify if this is the same number that @param c
	 * 
	 * @param c
	 * @return
	 */
	public boolean sameNumber(Card c) {
		return number.equals(c.getNumber());
	}

	/**
	 * @param c
	 * @return
	 */
	public boolean sameColor(Card c) {
		return color.equals(c.getColor());
	}

	/**
	 * Verify if the number of this is 8
	 * 
	 * @return
	 */
	public boolean is8() {
		return number.equals("8");
	}

	/**
	 * Verify if the this is an as
	 * 
	 * @return
	 */
	public boolean isAs() {
		return number.equals("1") || number.equals("A");
	}

	/**
	 * Verify if the this is a queen
	 * 
	 * @return
	 */
	public boolean isDame() {
		return number.equals("D") || number.equals("Q");
	}

	/**
	 * Textual format of a card
	 * 
	 * @return
	 */
	@Override
	public String toString() {
		return "[" + number + "," + color + "]";
	}

	public String getTitle() {
		if (isAs())
			return "As de " + getColor();
		else if (isDame())
			return "Dame de " + getColor();
		else if (getNumber().equals("R"))
			return "Roi de " + getColor();
		else if (getNumber().equals("V"))
			return "Vall\u00E9e de " + getColor();
		else
			return getNumber() + " de " + getColor();
	}

	/**
	 * @param ligne
	 * @return card
	 */
	public static Card createCard(String line) {
		line = line.replaceAll(" ", "");
		int i = line.indexOf(",");
		String numero = line.substring(1, i);
		String couleur = line.substring(i + 1, line.length() - 1);
		Card c = new Card(numero, couleur);
		return c;
	}

	public static LinkedList<Card> createCards(String line) throws Exception {
		if (!line
				.matches("\\[(1||A||7||8||9||10||K||R||J||V||D||Q),( +)?(pique||coeur||trefle||carreau)\\]((;( +)?\\[(1||A||7||8||9||10||K||R||J||V||D||Q),( +)?(pique||coeur||trefle||carreau)\\])+)?"))
			throw new Exception("Format incorrect");
		LinkedList<Card> cards = new LinkedList<Card>();
		String cs[] = line.split("; ?");
		for (String cardString : cs) {
			Card card = createCard(cardString);
			if (cards.contains(card))
				throw new Exception("Can't use a same card twice");
			cards.add(card);
		}
		return cards;
	}

	public static String encode(List<Card> cards) {
		if (cards == null || cards.isEmpty())
			return "";
		String s = cards.toString();
		return s.substring(1, s.length() - 1).replaceAll("\\],", "\\];");
	}

	/**
	 * @param top
	 * @return
	 */
	public static String encode(Card top) {
		if (top == null)
			return null;
		return top.toString();
	}

	public static LinkedList<Card> decode(String line) {
		try {
			return createCards(line);
		} catch (Exception e) {
			return new LinkedList<Card>();
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Card) {
			Card c = (Card) obj;
			return ((number == null ? c.getNumber() == null : number.equals(c
					.getNumber())) && (color == null ? c.getColor() == null
					: color.equals(c.getColor())));
		}
		return false;
	}

	@Override
	public int hashCode() {
		return 4 * poids + getColorIndex();
	}

	public static String listeNumero[] = { "1", "V", "D", "R", "7", "8", "9",
			"10" };
	public static String listeNumero2[] = { "A", "J", "Q", "K", "7", "8", "9",
			"10" };
	public static String listeCouleur[] = { "pique", "coeur", "trefle",
			"carreau" };

	public int getColorIndex() {
		if ("pique".equals(color))
			return 0;
		if ("coeur".equals(color))
			return 1;
		if ("trefle".equals(color))
			return 2;
		if ("carreau".equals(color))
			return 3;
		return -1;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Card neuft = new Card("8", "carreau");
			Card dixc = new Card("8", "pique");
			List<Card> cards;
			cards = createCards("[8,carreau]; [8,pique]");
			// cards.add(neuft);
			// cards.add(dixc);
			System.out.println(encode(cards));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public int compareTo(Card other) {
		if (poids == other.poids) {
			if (getColorIndex() == other.getColorIndex())
				return 0;
			if (getColorIndex() < other.getColorIndex())
				return -1;
			return 1;
		}
		if (poids < other.poids)
			return -1;
		return 1;
	}
}
