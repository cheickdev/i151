/**
 * 
 */
package shared;

import graphic.InterfaceClient;

import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

import packets.Packet;

/**
 * This class represent a player It contains a hand of the player.
 * 
 * @author sissoko
 * @date 22 déc. 2012 20:20:07
 */
public abstract class Player {

	/**
	 * The number of times a packet is send before the connection will be
	 * considered as broken.
	 */
	public String name;
	public Hand hand;
	public TableHand table;
	public Card topCard;
	public String ask;
	public boolean took;
	public boolean around;
	public boolean computer;
	public boolean begin;
	public String managerId;
	public boolean beginAnswered = false;

	public InterfaceClient client;

	/**
	 * Initializes this {@code TransportLayer} in CLOSED state. The specified
	 * layer will be used as the link to send and receive packets, but only once
	 * we have made a call to {@code bind}.
	 * 
	 * @param layer
	 *            the {@code Layer} used to send and receive.
	 * @see #bind(java.lang.String) bind
	 */
	// protected SocketChannel socket;

	/**
	 * 
	 * @param name
	 *            any player has a name that identify every player.
	 */
	public Player(String name) {
		this.name = name;
		this.computer = false;
		this.begin = true;
	}

	/**
	 * @param begin
	 *            the begin to set
	 */
	synchronized public void setBegin(boolean begin) {
		this.begin = begin;
		beginAnswered = true;
	}

	/**
	 * 
	 * @return true when the player terminates his hand. that means his hand is
	 *         empty
	 */
	public boolean isFinish() {
		return hand.isEmpty();
	}

	/**
	 * This function is called when the player decides to take or if he is
	 * obliged to do it. His hand will add all cards
	 * 
	 * Changes the value of took to true.
	 * 
	 * @param cards
	 */
	public void take(LinkedList<Card> cards) {
		took = true;
		hand.push(cards);
	}

	/**
	 * This function is called at start of the game. It adds the initials cards
	 * to the hand.
	 * 
	 * @param cards
	 */
	public void receive(LinkedList<Card> cards) {
		hand.push(cards);
	}

	/**
	 * Returns true when the player is in the state took
	 * 
	 * @return took
	 */
	public boolean isTook() {
		return took;
	}

	/**
	 * 
	 * @param around
	 */
	public void setTook(boolean around) {
		this.took = around;
	}

	/**
	 * If the player is allowed to play.
	 * 
	 * @return around
	 */
	public boolean isAround() {
		return around;
	}

	/**
	 * Change the around of the player and allowed the next player to play.
	 * 
	 * @return
	 */
	public boolean goAround() {
		if (!around)
			return false;
		if (!took)
			return false;
		return !(took = around = false);
	}

	public void unAsked() {
		ask = null;
	}

	abstract public boolean askBegin();

	/**
	 * 
	 * @param ask
	 * @return
	 */
	abstract public void ask();

	/**
	 * 
	 * @param top
	 * @return played cards
	 */
	abstract public void play(Card top);

	/**
	 * 
	 * @param ask
	 * @return played cards according to the ask.
	 */
	abstract public void play(String ask);

	/**
	 * 
	 * @param cards
	 * @return remove
	 */
	public List<Card> put(List<Card> cards) {
		return hand.push(cards);
	}

	/**
	 * 
	 * @param cards
	 * @return remove
	 */
	public List<Card> poll(List<Card> cards) {
		return hand.poll(cards);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * 
	 * @param around
	 */
	public void setAround(boolean around) {
		this.around = around;
	}

	/**
	 * 
	 * @return player name
	 */
	public String getName() {
		return name;
	}

	public boolean isClose() {
		return false;
	}

	/**
	 * 
	 * @return true when the player is an AiPlayer
	 */
	public boolean isComputer() {
		return computer;
	}

	public void receive(Packet packet) {
		throw new UnsupportedOperationException(
				"Veillez utiliser la méthode rédefinie");
	}

	/**
	 * 
	 * @param packet
	 */
	public void send(Packet packet) {
		throw new UnsupportedOperationException(
				"Veillez utiliser la méthode rédefinie");
	}

	/**
	 * 
	 * @param host
	 * @param port
	 * @param managerId
	 */
	public abstract boolean connect(String host, int port, String managerId);

	/**
	 * @param string
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Converts data from the Java native {@code String} encoding to a
	 * conventional encoding for effective networking.
	 * 
	 * @param data
	 *            a {@code String} to be encoded
	 * @return a {@code ByteBuffer} containing the encoded data
	 */
	static ByteBuffer encode(String data) {
		return Packet.CONVERTER.encode(data);
	}

	/**
	 * Encodes a {@link packets#Packet Packet} to a conventional encoding for
	 * effective networking.
	 * 
	 * @param packet
	 *            a {@code Packet} to be encoded
	 * @return a {@code ByteBuffer} containing the encoded packet
	 */
	static ByteBuffer encode(Packet packet) {
		return encode(packet.getEncoding());
	}

}
