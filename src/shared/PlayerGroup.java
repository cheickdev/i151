/**
 * 
 */
package shared;

import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import packets.Packet;
import packets.i151.AroundPacket;
import packets.i151.AskBeginPacket;
import packets.i151.AskPacket;
import packets.i151.AskedPacket;
import packets.i151.BankPacket;
import packets.i151.CardsPacket;
import packets.i151.ClosedPacket;
import packets.i151.PlayPacket;
import packets.i151.PlayerPacket;
import packets.i151.TookPacket;
import server.PlayerServer;

/**
 * @author sissoko
 * @date 21 févr. 2013 17:37:21
 */
public class PlayerGroup {

	enum State {
		PLAY, ASK, TOOKTWO, GAMEOVER, TOOK
	}

	/**
	 * If the new player is added successful
	 */
	public static final int ADDED = 0;

	/**
	 * If the game is started so the new player is added to a waiting list.
	 */
	public static final int WAITING = 1;

	/**
	 * number of players is 4 so no more player can be added.
	 */
	public static final int FULL = 2;

	/**
	 * If the player is already in group or the same name exist.
	 */
	public static final int EXIST = -1;

	private LinkedList<Card> playedCards;
	public boolean takeTwo, taked;
	private ArrayList<Player> players;
	private ArrayList<Player> waitingPlayers;
	Player current = null;
	private int capacity;
	private Deck deck;
	private String ask;
	private String id;
	private boolean started;
	private boolean beginAnswered;
	private boolean gameOver = true;

	State currentState;
	Object monitor;

	private boolean askWaiting;

	public PlayerGroup() {
		this(2);
	}

	public PlayerGroup(int capacity) {
		this.capacity = capacity;
		started = false;
		players = new ArrayList<Player>(capacity);
		waitingPlayers = new ArrayList<Player>();
		deck = new Deck();
		monitor = new Object();
	}

	public void shuffle() {
		deck.riffleShuffle(7);
	}

	public void distribute(int nbCards) {
		shuffle();
		nbCards = Math.min(nbCards, 32 / playerSize());
		for (int i = 0; i < playerSize(); i++) {
			Player p = get(i);
			List<Card> cards = deck.poll(nbCards);
			p.put(cards);
			p.send(new CardsPacket(id, p.name, Card.encode(cards), id));
			for (int j = 0; j < playerSize(); j++) {
				Player q = get(j);
				if (i != j) {
					q.send(new PlayerPacket(id, q.name, p.name, p.hand.size(),
							id));
				}
			}
		}
		for (int i = 0; i < playerSize(); i++) {
			Player p = get(i);
			p.send(new BankPacket(id, p.name, deck.size(), id));
		}
	}

	private boolean isValid(LinkedList<Card> played, Card topCard) {
		if (played.isEmpty())
			return false;
		boolean ok = all8(played);
		if (ok)
			return true;
		Card first = played.getFirst();
		if (topCard == null) {
			if (first.isDame())
				return isValidD(played, topCard);
			return played.size() == 1;
		}
		if (topCard.is8())
			return isValid(played, ask);
		if (first.isDame())
			return isValidD(played, topCard);
		if (played.size() == 1)
			return topCard.isEquivalent(first);
		if (!topCard.isEquivalent(first))
			return false;
		for (int i = 0; i < played.size(); i++) {
			if (!topCard.sameNumber(played.get(i)))
				return false;
		}
		return true;
	}

	private boolean isValidD(LinkedList<Card> played, Card topCard) {
		Card first = played.getFirst();
		if (!first.isDame())
			return false;
		if (topCard != null && !topCard.isEquivalent(first))
			return false;
		if (playerSize() != 2 && topCard != null && !topCard.isDame())
			return played.size() == 1;
		int endD = 0;
		for (; endD < played.size() && played.get(endD).isDame(); endD++)
			;
		if (endD == played.size())
			return true;
		if (topCard != null && topCard.isDame() && playerSize() != 2)
			return false;
		if (played.get(endD).is8())
			for (int i = endD + 1; i < played.size(); i++)
				if (!played.get(i).is8())
					return false;
		if (endD == played.size() - 1)
			return played.get(endD - 1).isEquivalent(played.get(endD));
		return false;
	}

	private boolean isValidD(LinkedList<Card> played, String ask) {
		Card first = played.getFirst();
		if (!first.isDame())
			return false;
		if (!first.getColor().equals(ask))
			return false;
		if (playerSize() != 2)
			return played.size() == 1;
		int endD = 0;
		for (; endD < played.size() && played.get(endD).isDame(); endD++)
			;
		if (endD == played.size())
			return true;
		if (played.get(endD).is8())
			for (int i = endD + 1; i < played.size(); i++)
				if (!played.get(i).is8())
					return false;
		if (endD == played.size() - 1)
			return played.get(endD - 1).isEquivalent(played.get(endD));
		return false;
	}

	private boolean isValid(LinkedList<Card> played, String ask) {
		if (played.isEmpty())
			return false;
		boolean ok = all8(played);
		if (ok)
			return true;
		Card first = played.getFirst();
		if (!first.getColor().equals(ask))
			return false;
		if (first.isDame())
			return isValidD(played, ask);
		return false;
	}

	private boolean all8(LinkedList<Card> played) {
		if (played.isEmpty())
			return false;
		for (int i = 0; i < played.size(); i++) {
			if (!played.get(i).is8())
				return false;
		}
		return true;
	}

	public int add(Player player) {
		if (players.contains(player))
			return EXIST;
		if (players.size() == capacity) {
			if (capacity + waitingPlayers.size() + 1 <= 4) {
				waitingPlayers.add(player);
				return WAITING;
			} else {
				return FULL;
			}
		}
		players.add(player);
		if (players.size() >= 2) {
			askBegin();
		}
		return ADDED;
	}

	public boolean remove(Player player) {
		return players.remove(player);
	}

	public boolean canStart() {
		return players.size() >= 2;
	}

	public boolean isFull() {
		return players.size() == capacity;
	}

	public boolean isFinish() {
		for (Player player : players) {
			if (player.isFinish())
				return true;
		}
		return false;
	}

	public Player getCurrent() {
		if (players.isEmpty())
			return null;
		current = players.get(0);
		for (Player player : players) {
			if (player.isAround()) {
				current = player;
			}
			player.setAround(false);
		}
		current.setAround(true);
		return current;
	}

	public Player getPlayer(String name) {
		for (Player player : players) {
			if (player.getName().equals(name))
				return player;
		}
		return null;
	}

	public String nextAround(String name) {
		Player player = getPlayer(name);
		int tour = 0;
		player.setAround(false);
		int pas = 1;
		if (deck.getTopCard() != null && deck.getTopCard().isDame()) {
			pas = 2;
		}
		tour = (indexOf(player) + pas) % players.size();
		Player aTour = players.get(tour);
		if (!(aTour.isClose() && isBilan())) {
			aTour.setAround(true);
			return aTour.getName();
		}
		return nextAround(aTour.getName());
	}

	private boolean isBilan() {
		return true;
	}

	private int indexOf(Player player) {
		return players.indexOf(player);
	}

	public String goAround() {
		String next = nextAround(getCurrent().getName());
		sendRoundPacket();
		return next;
	}

	public int playerSize() {
		return players.size();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// PlayerGroup pg = new PlayerGroup();
		// LinkedList<Card> cards = new LinkedList<>();
		// Card topCard = new Card("7", "pique");
		// cards.add(new Card("9", "pique"));
		// // cards.add(new Card("D", "carreau"));
		// // cards.add(new Card("D", "trefle"));
		// System.out.println(pg.isValid(cards, topCard));
		String cheik = "CheickMMMmMMMsissokoMMM";
		System.out.println(cheik.split("MMM").length);
	}

	public Player get(int i) {
		return players.get(i);
	}

	public void restart() {
		deck = new Deck();
		shuffle();
		clear();
		if (!waitingPlayers.isEmpty()) {
			capacity = capacity + waitingPlayers.size();
			players.addAll(waitingPlayers);
			waitingPlayers.clear();
		}
		distribute(8);
		if (current == null)
			getCurrent();
		else
			current.setAround(true);
		sendRoundPacket();
		current.play((Card) null);
	}

	public void end() {
		deck = new Deck();
		ask = null;
		unAsked();
	}

	/**
	 * 
	 */
	synchronized public void askBegin() {
		if (!started) {
			started = true;
		} else {
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				for (Player p : players) {
					p.send(new AskBeginPacket(id, p.name, id));
					boolean begin = p.askBegin();
					started = started && begin;
				}
				if (started) {
					restart();
				}
			}
		}).start();

	}

	public void asked(String asked) {
		for (Player p : players) {
			p.ask = asked;
			p.send(new AskedPacket(id, p.name, asked, id));
		}
	}

	public void unAsked() {
		for (Player player : players) {
			player.unAsked();
		}
	}

	public void clear() {
		for (Player player : players) {
			player.hand.clear();
		}
	}

	/**
	 * 
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param managerId
	 */
	public void setId(String managerId) {
		this.id = managerId;
	}

	/**
	 * 
	 * @param packet
	 */
	public void receive(Packet packet) {
		manage(packet);
	}

	/**
	 * 
	 * @param packet
	 */
	private void manage(Packet packet) {
		String asked;
		String source = packet.getSource();
		// String destination = packet.getDestination();
		Player player;
		switch (packet.getType()) {
		case ASKED:
			asked = packet.getContent();
			player = getPlayer(source);
			if (!current.equals(player)) {
				return;
			}
			if (!current.isAround()) {
				return;
			}
			this.ask = asked;
			asked(asked);
			goAround();
			current = getCurrent();
			current.play(ask);
			break;
		case BEGIN:
			boolean begin = "true".equalsIgnoreCase(packet.getContent());
			player = getPlayer(source);
			player.setBegin(begin);
			System.out.println(packet);
			break;
		case FAULT:
			break;
		case GO_AROUND:
			player = getPlayer(source);
			if (!current.equals(player)) {
				return;
			}
			if (!current.isTook()) {
				return;
			}
			goAround();
			current = getCurrent();
			if (deck.getTopCard() != null && deck.getTopCard().is8()) {
				current.play(ask);
			} else {
				current.play(deck.getTopCard());
			}
			break;
		case MESSAGE:
			for (Player p : players) {
				p.send(packet);
			}
			break;
		case PLAYED:
			try {
				LinkedList<Card> played = Card.createCards(packet.getContent());
				if (!isValid(played, deck.getTopCard())) {
					current.play(deck.getTopCard());
					return;
				}
				player = getPlayer(source);
				if (!current.equals(player)) {
					return;
				}
				if (!current.isAround()) {
					return;
				}
				current.poll(played);
				deck.putOnTable(played);
				current.setTook(false);
				playedCards = played;
				takeTwo = deck.getTopCard().isAs();
				if (deck.getTopCard().is8()) {
					if (current.isFinish()) {
						gameOver(true);
						deck.end();
						end();
						return;
					}
					current.send(new AskPacket(id, current.name, id));
					return;
				}
				goAround();
				ask = null;
				askWaiting = false;
				current = getCurrent();
				current.play(deck.getTopCard());
			} catch (Exception e) {
				// System.err.println(packet);
				e.printStackTrace();
			}
			break;
		case RAW:
			break;
		case TAKE:
			take(current.name);
			break;
		case SET_CARDS:
			distribute(8);
			break;
		default:
			break;
		}
	}

	private void test(Packet packet) {
		switch (packet.getType()) {
		case ASK:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(packet);
			}
			break;
		case ASKED:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(packet);
			}
			break;
		case FAULT:

			break;
		case GO_AROUND:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(new PlayPacket("server", p.getName(), "[J, pique]", id));
			}
			break;
		case PLAY:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(packet);
			}
			break;
		case MESSAGE:

			break;
		case PLAYED:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(packet);
			}
			break;
		case SET_CARDS:
			System.out.println("playerGroup ::" + id + ":: " + packet);

			// for (Player p : players) {
			// p.receive(packet);
			// }
			distribute(8);
			break;
		case TAKE:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(new TookPacket(packet.getSource(), packet
						.getDestination(), "[K, trefle]; [K, pique]", id));
			}
			break;
		case TOOK:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(packet);
			}
			break;
		case CLOSED:
			break;
		case CONNECTION:
			break;
		case JOIN_GROUP:
			break;
		case SET_MANAGER_ID:
			break;
		case SET_NAME:
			System.out.println("playerGroup ::" + id + ":: " + packet);
			for (Player p : players) {
				p.send(packet);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * @param sc
	 */
	public void remove(SocketChannel sc) {
		if (sc == null)
			return;
		System.out.println("UP : " + players.size());
		for (Player p : players) {
			if (sc.equals(((PlayerServer) p).socket)) {
				deck.push(p.hand);
				players.remove(p);
				receive(new ClosedPacket("server", "*ALL*", p.getName(), id));
				break;
			}
		}
		System.out.println("DOWN : " + players.size());
	}

	private void gameOver(boolean b) {
		this.gameOver = b;
	}

	/**
	 * 
	 * @param name
	 */
	private void take(String name) {
		if (!current.isTook() && deck.getTopCard() != null) {
			int nbCarte = 1;
			if (takeTwo) {
				nbCarte = 2;
				takeTwo = false;
			}
			take(name, nbCarte);
		}
	}

	/**
	 * 
	 * @param name
	 * @param nbCard
	 */
	private void take(String name, int nbCard) {
		LinkedList<Card> cards = deck.poll(nbCard);
		current.receive(cards);
		current.send(new TookPacket(id, current.name, Card.encode(cards), id));
		current.setTook(true);
		if (nbCard == 2) {
			goAround();
			current.setTook(false);
		}
	}

	private void sendRoundPacket() {
		for (Player p : players) {
			p.send(new AroundPacket(id, p.name, p.around, id));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id + "::" + playerSize();
	}
}
