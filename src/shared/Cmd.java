package shared;

public class Cmd
{
    public static final String
        A_DEPOSER="A_DEPOSER",
        A_GAGNER="A_GAGNER",
        A_PIOCHER="A_PIOCHER",
        A_DEMANDER_COULEUR="A_DEMANDER_COULEUR",
        DEMANDER_COULEUR="DEMANDER_COULEUR",
        DEMANDER_DEMARRER_NOUVELLE_PARTIE="DEMANDER_DEMARRER_NOUVELLE_PARTIE",
        JOUER="JOUER",
        PIOCHER="PIOCHER",
        POINTS="POINTS",
        SET_CARTES="SET_CARTES",
        SET_NB_CARTES_BANQUE="SET_NB_CARTES_BANQUE",
        DEMANDER_TERMINER_PARTIE="DEMANDER_TERMINER_PARTIE",
        MESSAGE="MESSAGE",
        SET_ID="SET_ID",
        PASSER_TOUR="PASSER_TOUR",
        GET_NOM="GET_NOM",
        SET_NOM="SET_NOM",
        DECONNECTION = "DECONNECTION",
        		FAUTE = "FAUTE",
        		BILAN = "BILAN";
        
    
    public static final String
        FIN_CMD = "##",
        FIN_CMD_N = "##\n";

    public static String cartePath = "Cartes/",
    		E = ".jpg";

}
