/**
 * 
 */
package shared;

/**
 * @author Sissoko
 * @date 21 avr. 2014 18:03:11
 */
public enum CardColor {
	PIQUE, P, COEUR, C, TREFLE, T, CARREAU, K
}
