/**
 * 
 */
package shared;

import java.util.LinkedList;

/**
 * @author sissoko
 * @date 21 févr. 2013 18:19:43
 */
public class Manager implements Runnable {

	enum State {

	}

	private PlayerGroup playerGroup;
	private Deck deck;
	private boolean gameOver = true;
	private String ask;
	private LinkedList<Card> playedCards;
	public boolean takeTwo, taked;
	private Player current = null;
	State currentState;
	private String id;

	public Manager(PlayerGroup playerGroup) {
		this.playerGroup = playerGroup;
		deck = new Deck();
	}

	public void shuffle() {
		deck.riffleShuffle(8);
	}

	public void distribute(int nbCards) {
		nbCards = Math.min(nbCards, 32 / playerGroup.playerSize());
		for (int i = 0; i < playerGroup.playerSize(); i++) {
			playerGroup.get(i).receive(deck.poll(nbCards));
		}
	}

	public void play(String name) {
		current = playerGroup.getPlayer(name);
		LinkedList<Card> played = null;// current.play(deck.getTopCard());
		System.out.println(current.getName());
		if (played == null) {
			piocher(name);
			return;
		}
		System.out.println(played);
		if (isValid(played, deck.getTopCard())) {
			if (!current.isAround()) {
				return;
			}
			current.poll(played);
			deck.putOnTable(played);
			current.setTook(false);
			playedCards = played;
			takeTwo = deck.getTopCard().isAs();
			if (deck.getTopCard().is8()) {
				if (current.isFinish()) {
					gameOver(true);
					deck.end();
					end();
					return;
				}
				if (current.isComputer()) {
					ask = null;// current.ask(null);
					// playerGroup.asked(ask);
					playerGroup.goAround();
					if (playerGroup.getCurrent().isComputer()) {
						Player cplayer = playerGroup.getCurrent();
						play(cplayer.getName());
					}
					return;
				}
			} else if (deck.getTopCard().isDame()) {
				ask = null;
				playerGroup.unAsked();
				if (current.isFinish()) {
					if (playerGroup.playerSize() == 2) {
						take(current.getName(), 1);
						current.setTook(true);
					} else {
						gameOver(true);
						deck.end();
						end();
						return;
					}
				} else {
					String tour = playerGroup.nextAround(current.getName());
					tour = playerGroup.nextAround(tour);
					if (playerGroup.getCurrent().isComputer()) {
						Player cplayer = playerGroup.getCurrent();
						if (!gameOver) {
							play(cplayer.getName());
						}
					}
				}
			} else {
				ask = null;
				playerGroup.unAsked();
				if (current.isFinish()) {
					gameOver(true);
					deck.end();
					end();
					return;
				}
				current.setTook(false);
				playerGroup.goAround();
				if (playerGroup.getCurrent().isComputer()) {
					current = playerGroup.getCurrent();
					System.out.println(current.getName());
					if (!playerGroup.isFinish()) {
						if (!gameOver) {
							play(current.getName());
						}
					}
				}

			}
		}
	}

	private void end() {
		deck = new Deck();
		ask = null;
		playerGroup.unAsked();
		// TODO
	}

	private void gameOver(boolean b) {
		this.gameOver = b;
	}

	public void restart() {
		deck = new Deck();
		shuffle();
		playerGroup.clear();
		distribute(8);
		if (current == null)
			current = playerGroup.getCurrent();
		else
			current.setAround(true);
		if (current.isComputer()) {
			play(current.getName());
		}
	}

	private boolean isValid(LinkedList<Card> played, Card topCard) {
		boolean ok = all8(played);
		if (ok)
			return true;
		Card first = played.getFirst();
		if (topCard == null) {
			if (first.isDame())
				return isValidD(played, topCard);
			return played.size() == 1;
		}
		if (topCard.is8())
			return isValid(played, ask);
		if (first.isDame())
			return isValidD(played, topCard);
		if (played.size() == 1)
			return topCard.isEquivalent(first);
		if (!topCard.isEquivalent(first))
			return false;
		for (int i = 0; i < played.size(); i++) {
			if (!topCard.sameColor(played.get(i)))
				return false;
		}
		return true;
	}

	private boolean isValidD(LinkedList<Card> played, Card topCard) {
		Card first = played.getFirst();
		if (!first.isDame())
			return false;
		if (topCard != null && !topCard.isEquivalent(first))
			return false;
		if (playerGroup.playerSize() != 2 && topCard != null
				&& !topCard.isDame())
			return played.size() == 1;
		int endD = 0;
		for (; endD < played.size() && played.get(endD).isDame(); endD++)
			;
		if (endD == played.size())
			return true;
		if (topCard != null && topCard.isDame()
				&& playerGroup.playerSize() != 2)
			return false;
		if (played.get(endD).is8())
			for (int i = endD + 1; i < played.size(); i++)
				if (!played.get(i).is8())
					return false;
		if (endD == played.size() - 1)
			return played.get(endD - 1).isEquivalent(played.get(endD));
		return false;
	}

	private boolean isValidD(LinkedList<Card> played, String ask) {
		Card first = played.getFirst();
		if (!first.isDame())
			return false;
		if (!first.getColor().equals(ask))
			return false;
		if (playerGroup.playerSize() != 2)
			return played.size() == 1;
		int endD = 0;
		for (; endD < played.size() && played.get(endD).isDame(); endD++)
			;
		if (endD == played.size())
			return true;
		if (played.get(endD).is8())
			for (int i = endD + 1; i < played.size(); i++)
				if (!played.get(i).is8())
					return false;
		if (endD == played.size() - 1)
			return played.get(endD - 1).isEquivalent(played.get(endD));
		return false;
	}

	private boolean isValid(LinkedList<Card> played, String ask) {
		boolean ok = all8(played);
		if (ok)
			return true;
		Card first = played.getFirst();
		if (!first.getColor().equals(ask))
			return false;
		if (first.isDame())
			return isValidD(played, ask);
		return false;
	}

	private boolean all8(LinkedList<Card> played) {
		for (int i = 0; i < played.size(); i++) {
			if (!played.get(i).is8())
				return false;
		}
		return true;
	}

	private void piocher(String name) {
		if (!current.isTook() && deck.getTopCard() != null) {
			int nbCarte = 1;
			if (takeTwo) {
				nbCarte = 2;
				takeTwo = false;
			}
			take(name, nbCarte);
		}
	}

	private void take(String name, int nbCard) {
		LinkedList<Card> cards = deck.poll(nbCard);
		current.receive(cards);
		current.setTook(true);
		if (nbCard == 2) {
			playerGroup.goAround();
			current.setTook(false);
			Player player = playerGroup.getCurrent();
			if (player.isComputer()) {
				if (!gameOver)
					play(player.getName());
			}
		} else if (current.isComputer()) {
			if (!gameOver)
				play(current.getName());
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 * @return id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param managerId
	 */
	public void setId(String managerId) {
		this.id = managerId;
	}
}
