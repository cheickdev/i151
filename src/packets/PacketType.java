package packets;

import packets.Packet.Field;
import packets.i151.AroundPacket;
import packets.i151.AskBeginPacket;
import packets.i151.AskPacket;
import packets.i151.AskedPacket;
import packets.i151.BankPacket;
import packets.i151.BeginPacket;
import packets.i151.CardsPacket;
import packets.i151.ClosedPacket;
import packets.i151.ConnectionPacket;
import packets.i151.ExistPacket;
import packets.i151.FullPacket;
import packets.i151.GoAroundPacket;
import packets.i151.JoinGroupPacket;
import packets.i151.ManagerIdPacket;
import packets.i151.ManagerInfoPacket;
import packets.i151.MessagePacket;
import packets.i151.NamePacket;
import packets.i151.PlayPacket;
import packets.i151.PlayedPacket;
import packets.i151.PlayerNamePacket;
import packets.i151.PlayerPacket;
import packets.i151.TakePacket;
import packets.i151.TookPacket;
import packets.i151.WaitingPacket;

/**
 * This enum specifies the declared types for formatted packets and implements
 * some utilities for formatting packets and checking if packets are well
 * formatted.
 * <p>
 * Any packet has a source, a destination and a type. Depending on its type, a
 * packet may also have a numeric field (a positive integer used for control)
 * and other additional fields.
 * 
 * @see Packet
 * 
 * @author Philippe Chassignet
 * @author INF557, DIX, � 2010-2012 �cole Polytechnique
 * @version 1.2, 2012/12/07
 */

public enum PacketType {

	/**
	 * Constant to type a {@link RawPacket}. A {@code RawPacket} is used
	 * internally as a wrapper to keep track of a badly formatted packet. For
	 * such a packet, even the required fields, as source, destination and type
	 * may be undefined.
	 */
	RAW(-3) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			throw new IllegalStateException();
		}
	},
	/**
	 * Constant to type a {@link AskPacket}. A {@code AskPacket} is used at the
	 * transport layer and it is sent to transmit a wrapped piece of ask. Such a
	 * packet has two additional fields, a sequence number and the piece of data
	 * as the content field.
	 */
	ASK(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new AskPacket(source, destination, Packet.getField(
						Field.CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * On demande à la destinateur de joueur.
	 */
	PLAY(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new PlayPacket(source, destination, Packet.getField(
						Field.CONTENT, args), Packet.getField(
						Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * La source demande à prendre à la banque.
	 */
	TAKE(1) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new TakePacket(source, destination, Packet.getField(
						Field.CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Permet de passer le tour.
	 */
	GO_AROUND(1) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new GoAroundPacket(source, destination, Packet.getField(
						Field.CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Un message que la source envoie à tout le monde.
	 */
	MESSAGE(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new MessagePacket(source, destination, Packet.getField(
						Field.CONTENT, args), Packet.getField(
						Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Le type de packet pour annoncer une faute.
	 */
	FAULT(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			// TODO Auto-generated method stub
			try {
				return null;
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Quand un joueur demande une couleur on le met dans ce paquet pour
	 * l'envoyer à tout le monde!
	 */
	ASKED(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new AskedPacket(source, destination, Packet.getField(
						Field.CONTENT, args), Packet.getField(
						Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Les cartes jouées pas le joueur source.
	 */
	PLAYED(3) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new PlayedPacket(source, destination, Packet.getField(
						Field.CONTENT, args), Packet.getField(
						Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * La reponse de la requete TAKE.
	 */
	TOOK(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new TookPacket(source, destination, Packet.getField(
						Field.CONTENT, args), Packet.getField(
						Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Le type de packet pour distribuer les cartes inialement.
	 */
	SET_CARDS(2) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new CardsPacket(source, destination, Packet.getField(
						Field.CONTENT, args), Packet.getField(
						Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Met à jour le nom du groupe de joueurs.
	 */
	SET_MANAGER_ID(1) {
		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new ManagerIdPacket(source, destination,
						Packet.getField(Field.CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}

	},
	/**
	 * Le type de packet pour rejoindre un groupe.
	 */
	JOIN_GROUP(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new JoinGroupPacket(source, destination,
						Integer.parseInt(Packet.getField(Field.CONTENT, args)),
						Packet.getField(Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}

	},
	/**
	 * La connexion
	 */
	CONNECTION(2) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			try {
				return new ConnectionPacket(source, destination,
						Packet.getField(Field.CONTENT, args), Packet.getField(
								Field.SECOND_CONTENT, args));
			} catch (Exception e) {
				return new RawPacket(rawPacket);
			}
		}
	},
	/**
	 * Met à jour le nom du joueur.
	 */
	SET_NAME(2) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new NamePacket(source, destination, Packet.getField(
					Field.CONTENT, args), Packet.getField(Field.SECOND_CONTENT,
					args));
		}

	},
	CLOSED(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new ClosedPacket(source, destination, Packet.getField(
					Field.CONTENT, args), Packet.getField(Field.SECOND_CONTENT,
					args));
		}

	},
	/**
	 * Le type de packet transportant le nombre de carte à la banque.
	 */
	BANK_COUNT(2) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new BankPacket(source, destination, Integer.parseInt(Packet
					.getField(Field.CONTENT, args)), Packet.getField(
					Field.SECOND_CONTENT, args));
		}

	},
	/**
	 * Le nombre de carte que les autres joueurs ont.
	 */
	PLAYER_COUNT(3) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new PlayerPacket(source, destination, Packet.getField(
					Field.CONTENT, args), Integer.parseInt(Packet.getField(
					Field.SECOND_CONTENT, args)), Packet.getField(
					Field.CONTENT.ordinal() + 2, args));
		}

	},
	/**
	 * Envoie le nom du joueur.
	 */
	PLAYER_NAME(2) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new PlayerNamePacket(source, destination, Packet.getField(
					Field.CONTENT, args), Packet.getField(Field.SECOND_CONTENT,
					args));
		}

	},
	/**
	 * Contient la valeur du tour du joueur.
	 */
	AROUND(2) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new AroundPacket(source, destination,
					"true".equalsIgnoreCase(Packet
							.getField(Field.CONTENT, args)), Packet.getField(
							Field.SECOND_CONTENT, args));
		}

	},
	/**
	 * Demande de commencer une nouvelle partie.
	 */
	ASK_BEGIN(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new AskBeginPacket(source, destination, Packet.getField(
					Field.CONTENT, args));
		}

	},
	/**
	 * La reponse à la requete ASK_BEGIN
	 */
	BEGIN(2) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new BeginPacket(source, destination,
					"true".equalsIgnoreCase(Packet
							.getField(Field.CONTENT, args)), Packet.getField(
							Field.SECOND_CONTENT, args));
		}

	},
	/**
	 * Quand le joueur a été ajouté dans la listend'attente.
	 */
	WAITING(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new WaitingPacket(source, destination, Packet.getField(
					Field.CONTENT, args));
		}

	},
	/**
	 * Quand le nom du joueur exist déjà dans le groupe.
	 */
	EXIST(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new ExistPacket(source, destination, Packet.getField(
					Field.CONTENT, args));
		}

	},
	/**
	 * Quand le groupe est plein..
	 */
	FULL(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new FullPacket(source, destination, Packet.getField(
					Field.CONTENT, args));
		}

	},
	/**
	 * Quand le groupe est plein..
	 */
	MANAGER_INFO(1) {

		@Override
		public Packet makePacket(String source, String destination,
				String[] args, String rawPacket) {
			return new ManagerInfoPacket(source, destination, Packet.getField(
					Field.CONTENT, args));
		}

	};

	private final int nFields;

	private PacketType(int n) {
		nFields = n;
	}

	/**
	 * Returns the number of fields specified for this type of packet.
	 * 
	 * @return the number of fields specified for this type of packet
	 */
	public int fieldsCount() {
		return nFields;
	}

	/**
	 * Returns a new instance of the subclass of {@code Packet} that corresponds
	 * to this type of packet. Any type constant declared in the present enum
	 * must implement this method in order to check the parameter values and
	 * pass them to the appropriate constructor. For convenience, the parameters
	 * have some redundancy.<br/>
	 * 
	 * @param source
	 *            value for the source field
	 * @param destination
	 *            value for the destination field
	 * @param args
	 *            the whole set of fields that must be given in the correct
	 *            order.
	 * @param rawPacket
	 *            a raw representation of the packet, used to build a
	 *            {@code RawPacket} in case of bad field values are given
	 * 
	 * @return a corresponding instance of {@code Packet}
	 */
	abstract public Packet makePacket(String source, String destination,
			String[] args, String rawPacket);

}
