/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 11 nov. 2013 18:51:36
 */
public class TakePacket extends Packet {

	/**
	 * @param source
	 * @param destination
	 * @param type
	 * @param numeric
	 * @param extraFields
	 */
	public TakePacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.TAKE,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
