/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 11 nov. 2013 18:56:34
 */
public class TookPacket extends Packet {

	/**
	 * @param source
	 * @param destination
	 * @param type
	 * @param numeric
	 * @param extraFields
	 */
	public TookPacket(String source, String destination, String cards,
			String managerId) {
		super(source, destination, PacketType.TOOK, new String[] {
				cards == null ? "" : cards.replace(DELIMITER, " "),
				managerId == null ? "" : managerId.replace(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
