/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 7 mai 2014 22:08:15
 */
public class AroundPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param around
	 * @param managerId
	 */
	public AroundPacket(String source, String destination, boolean around,
			String managerId) {
		super(source, destination, PacketType.AROUND, new String[] {
				around + "",
				managerId == null ? "" : managerId.replace(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
