/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 10 nov. 2013 00:46:25
 */
public class JoinGroupPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param managerId
	 */
	public JoinGroupPacket(String source, String destination, int capacity,
			String managerId) {
		super(source, destination, PacketType.JOIN_GROUP, new String[] {
				capacity + "",
				managerId == null ? "" : managerId.replace(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
