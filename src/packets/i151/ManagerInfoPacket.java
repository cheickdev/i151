/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 10 mai 2014 04:24:09
 */
public class ManagerInfoPacket extends Packet {
	/**
 * 
 */
	public ManagerInfoPacket(String source, String destination, String infos) {
		super(source, destination, PacketType.MANAGER_INFO,
				new String[] { infos == null ? "" : infos.replace(DELIMITER,
						" ") });
	}
}
