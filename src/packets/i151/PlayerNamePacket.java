/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 4 mai 2014 02:30:14
 */
public class PlayerNamePacket extends Packet {
	/**
	 * 
	 */
	public PlayerNamePacket(String source, String destination, String name,
			String managerId) {
		super(source, destination, PacketType.PLAYER_NAME, new String[] {
				name == null ? "" : name.replaceAll(DELIMITER, " "),
				managerId == null ? "" : managerId.replaceAll(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
