/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 7 nov. 2013 10:03:09
 */
public class ManagerIdPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param managerId
	 */
	public ManagerIdPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.SET_MANAGER_ID,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
