/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 8 mai 2014 14:31:36
 */
public class WaitingPacket extends Packet {
	/**
	 * 
	 * @param source
	 * @param destination
	 * @param managerId
	 */
	public WaitingPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.WAITING,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
