/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 4 mai 2014 02:30:14
 */
public class BankPacket extends Packet {
	/**
	 * 
	 */
	public BankPacket(String source, String destination, int count,
			String managerId) {
		super(source, destination, PacketType.BANK_COUNT, new String[] {
				count + "",
				managerId == null ? "" : managerId.replaceAll(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
