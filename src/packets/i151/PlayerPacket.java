/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 4 mai 2014 02:30:14
 */
public class PlayerPacket extends Packet {
	/**
	 * 
	 */
	public PlayerPacket(String source, String destination, String playerId,
			int count, String managerId) {
		super(source, destination, PacketType.PLAYER_COUNT, new String[] {
				playerId == null ? "" : playerId.replaceAll(DELIMITER, " "),
				count + "",
				managerId == null ? "" : managerId.replaceAll(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(2);
	}
}
