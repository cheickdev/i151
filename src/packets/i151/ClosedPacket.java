/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 11 nov. 2013 12:55:13
 */
public class ClosedPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param name
	 */
	public ClosedPacket(String source, String destination, String name,
			String managerId) {
		super(source, destination, PacketType.CLOSED, new String[] {
				name == null ? "" : name.replace(DELIMITER, " "),
				managerId == null ? "" : managerId.replace(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
