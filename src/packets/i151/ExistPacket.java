/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 8 mai 2014 15:25:57
 */
public class ExistPacket extends Packet {
	/**
	 * 
	 * @param source
	 * @param destination
	 * @param managerId
	 */
	public ExistPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.EXIST,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
