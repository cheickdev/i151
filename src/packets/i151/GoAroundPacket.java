/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 11 nov. 2013 19:02:39
 */
public class GoAroundPacket extends Packet {

	/**
	 * @param source
	 * @param destination
	 * @param type
	 * @param numeric
	 * @param extraFields
	 */
	public GoAroundPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.GO_AROUND,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}

}
