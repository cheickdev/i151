/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 10 nov. 2013 02:51:18
 */
public class AskedPacket extends Packet {

	/**
	 * @param source
	 * @param destination
	 * @param type
	 * @param numeric
	 * @param extraFields
	 */
	public AskedPacket(String source, String destination, String asked,
			String managerId) {
		super(source, destination, PacketType.ASKED, new String[] {
				asked == null ? "" : asked.replaceAll(DELIMITER, " "),
				managerId == null ? "" : managerId.replaceAll(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}

}
