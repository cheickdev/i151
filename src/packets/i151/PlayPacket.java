/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 4 nov. 2013 14:27:06
 */
public class PlayPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param type
	 */
	public PlayPacket(String source, String destination, String topCard,
			String managerId) {
		super(source, destination, PacketType.PLAY, new String[] {
				topCard == null ? "" : topCard.replace(DELIMITER, " "),
				managerId == null ? "" : managerId.replace(DELIMITER, " "), });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}

}
