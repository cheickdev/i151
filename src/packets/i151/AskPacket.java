/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 4 nov. 2013 01:11:03
 */
public class AskPacket extends Packet {

	/**
	 * Constructs a {@code LinkPacket}.
	 * 
	 * @param source
	 *            the source of the packet
	 * @param destination
	 *            the destination of the packet
	 */
	public AskPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.ASK,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
