/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 8 mai 2014 13:10:46
 */
public class BeginPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param begin
	 * @param managerId
	 */
	public BeginPacket(String source, String destination, boolean begin,
			String managerId) {
		super(source, destination, PacketType.BEGIN, new String[] { begin + "",
				managerId == null ? "" : managerId.replace(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}
}
