/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 8 mai 2014 13:04:57
 */
public class AskBeginPacket extends Packet {

	/**
	 * 
	 * @param source
	 * @param destination
	 * @param managerId
	 */
	public AskBeginPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.ASK_BEGIN,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
