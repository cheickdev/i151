/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 11 nov. 2013 18:46:37
 */
public class CardsPacket extends Packet {

	/**
	 * @param source
	 * @param destination
	 * @param type
	 * @param numeric
	 * @param extraFields
	 */
	public CardsPacket(String source, String destination, String cards,
			String managerId) {
		super(source, destination, PacketType.SET_CARDS, new String[] {
				cards == null ? "" : cards.replace(DELIMITER, " "),
				managerId == null ? "" : managerId.replace(DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent(1);
	}

}
