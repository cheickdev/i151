/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 10 nov. 2013 03:00:19
 */
public class ConnectionPacket extends Packet {

	/**
	 * @param source
	 * @param destination
	 * @param type
	 * @param numeric
	 * @param extraFields
	 */
	public ConnectionPacket(String source, String destination, String host,
			String port) {
		super(source, destination, PacketType.CONNECTION, new String[] { host,
				port });
	}

}
