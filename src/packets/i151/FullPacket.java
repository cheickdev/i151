/**
 * 
 */
package packets.i151;

import packets.Packet;
import packets.PacketType;

/**
 * @author Sissoko
 * @date 10 mai 2014 03:47:28
 */
public class FullPacket extends Packet {
	/**
	 * 
	 * @param source
	 * @param destination
	 * @param managerId
	 */
	public FullPacket(String source, String destination, String managerId) {
		super(source, destination, PacketType.WAITING,
				new String[] { managerId == null ? "" : managerId.replace(
						DELIMITER, " ") });
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see packets.Packet#getManagerId()
	 */
	@Override
	public String getManagerId() {
		return getContent();
	}
}
