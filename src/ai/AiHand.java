/**
 * 
 */
package ai;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import shared.Card;
import shared.Hand;

/**
 * @author sissoko
 * @date 20 févr. 2013 19:18:29
 */
public class AiHand extends Hand {

	private static final int SIZE = 8;
	private Card[][] hands;

	public AiHand() {
		hands = new Card[8][4];
		for (int i = 0; i < 8; i++) {
			hands[i] = new Card[4];
		}
	}

	@Override
	public Card get(int i) {
		return hands[i / 4][i % 4];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#poll(java.util.LinkedList)
	 */
	@Override
	public List<Card> poll(List<Card> cards) {
		for (Card card : cards) {
			hands[card.getPoids()][card.getColorIndex()] = null;
		}
		return cards;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#push(java.util.LinkedList)
	 */
	@Override
	public List<Card> push(List<Card> cards) {
		for (Card card : cards) {
			hands[card.getPoids()][card.getColorIndex()] = card;
		}
		return cards;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#poll(shared.Card)
	 */
	@Override
	public Card poll(Card card) {
		hands[card.getPoids()][card.getColorIndex()] = null;
		return card;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#push(shared.Card)
	 */
	@Override
	public Card push(Card card) {
		hands[card.getPoids()][card.getColorIndex()] = card;
		return card;
	}

	public LinkedList<Card> getSameNumberForTop(Card top) {
		if (top == null)
			return new LinkedList<Card>();
		LinkedList<Card> cards = new LinkedList<Card>();
		for (int i = 0; i < hands[top.getPoids()].length; i++)
			if (hands[top.getPoids()][i] != null)
				cards.add(hands[top.getPoids()][i]);
		return cards;
	}

	public LinkedList<Card> getSameColorForTop(Card top) {
		if (top == null)
			return new LinkedList<Card>();
		LinkedList<Card> cards = new LinkedList<Card>();
		if (!top.isAs())
			for (int i = 0; i < hands.length; i++)
				if (hands[i][top.getColorIndex()] != null
						&& !hands[i][top.getColorIndex()].is8())
					cards.add(hands[i][top.getColorIndex()]);

		return cards;
	}

	public LinkedList<Card> getAll8() {
		LinkedList<Card> cards = new LinkedList<Card>();
		for (int i = 0; i < hands[SIZE - 1].length; i++)
			if (hands[SIZE - 1][i] != null)
				cards.add(hands[SIZE - 1][i]);
		return cards;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < hands[i].length; j++)
				if (hands[i][j] != null)
					return false;
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Hand#size()
	 */
	@Override
	public int size() {
		int size = 0;
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < hands[i].length; j++)
				if (hands[i][j] != null)
					size += 1;
		}
		return size;
	}

	@Override
	public boolean contains(Card card) {
		return hands[card.getPoids()][card.getColorIndex()] != null;
	}

	public String toString() {
		String res = "[";
		for (int i = 0; i < SIZE - 1; i++) {
			res += Arrays.toString(hands[i]) + ", ";
		}
		return res + Arrays.toString(hands[SIZE - 1]) + "]";
	}

	public static void main(String[] args) {
		AiHand hand = new AiHand();
	}

	public LinkedList<Card> findFirst() {
		LinkedList<Card> cards = new LinkedList<Card>();
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < hands[i].length; j++) {
				if (hands[i][j] != null) {
					cards.add(hands[i][j]);
					return cards;
				}
			}
		}
		return null;
	}

	@Override
	public void clear() {
		hands = new Card[8][4];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<Card> iterator() {

		return null;
	}
}
