/**
 * 
 */
package ai;

import java.util.LinkedList;

import shared.Card;
import shared.Player;

/**
 * @author sissoko
 * @date 22 déc. 2012 20:38:21
 */
public class AiPlayer extends Player {

	AiHand aiHand;

	public AiPlayer(String name) {
		super(name);
		hand = new AiHand();
		aiHand = (AiHand) hand;
		computer = true;
	}

	@Override
	public void play(Card top) {
		if (ask != null)
			// return play(ask);
			if (top == null) {
				// return aiHand.findFirst();
			}
		LinkedList<Card> sameNumber = aiHand.getSameNumberForTop(top);
		LinkedList<Card> sameColor = aiHand.getSameColorForTop(top);
		if (!sameNumber.isEmpty()) {
			// return sameNumber;
		}
		LinkedList<Card> cards = new LinkedList<Card>();
		if (!sameColor.isEmpty()) {
			cards.add(sameColor.getFirst());
			// return cards;
		}
		LinkedList<Card> all8 = aiHand.getAll8();
		if (!all8.isEmpty()) {
			cards.add(all8.getFirst());
			// return cards;
		}
		// return null;

	}

	@Override
	public void play(String ask) {
		LinkedList<Card> cards = new LinkedList<Card>();
		LinkedList<Card> sameColor = aiHand.getSameColorForTop(new Card("-1",
				ask));
		if (!sameColor.isEmpty()) {
			cards.add(sameColor.getFirst());
			// return cards;
		}
		LinkedList<Card> all8 = aiHand.getAll8();
		if (!all8.isEmpty()) {
			cards.add(all8.getFirst());
			// return cards;
		}
		// return null;
	}

	@Override
	public void ask() {
		if (ask == null) {
			ask = aiHand.findFirst().getFirst().getColor();
		}
		// return ask;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#connect(java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean connect(String host, int port, String managerId) {
		// TODO Auto-generated method stub
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#askBegin()
	 */
	@Override
	public boolean askBegin() {
		// TODO Auto-generated method stub
		return false;
	}

}
