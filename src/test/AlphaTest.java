/**
 * 
 */
package test;

import shared.ConsolePlayer;
import shared.Manager;
import shared.Player;
import shared.PlayerGroup;
import ai.AiPlayer;

/**
 * @author Sissoko
 * @date 3 nov. 2013 22:31:04
 */
public class AlphaTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Player ai = new AiPlayer("Mac1");
		Player console = new ConsolePlayer("Console1");
		PlayerGroup pg = new PlayerGroup();
		pg.add(ai);
		pg.add(console);
		Manager manager = new Manager(pg);
		manager.run();
	}
}
