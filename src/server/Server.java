/**
 * 
 */
package server;

import static packets.Packet.BUFFER_SIZE;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import packets.Packet;
import packets.PacketType;
import packets.i151.AskPacket;
import packets.i151.AskedPacket;
import packets.i151.CardsPacket;
import packets.i151.ExistPacket;
import packets.i151.FullPacket;
import packets.i151.JoinGroupPacket;
import packets.i151.ManagerIdPacket;
import packets.i151.MessagePacket;
import packets.i151.NamePacket;
import packets.i151.PlayPacket;
import packets.i151.PlayedPacket;
import packets.i151.TookPacket;
import packets.i151.WaitingPacket;
import shared.PlayerGroup;

import common.Keyboard;

/**
 * @author sissoko
 * @date 20 déc. 2012 22:20:34
 */
public class Server implements Runnable {

	public static int createdGroup = -1;
	protected static final Charset CONVERTER = Charset.availableCharsets().get(
			"UTF-8");
	public Map<String, PlayerGroup> groups = new HashMap<String, PlayerGroup>();
	ServerSocketChannel server;
	Selector selector;
	int port;

	/**
	 * @param port
	 * @throws IOException
	 */
	public Server(int port) throws IOException {
		this.port = port;
		initServer(port);
	}

	/**
	 * 
	 * @param port
	 */
	void initServer(int port) {
		try {
			server = ServerSocketChannel.open();
			server.configureBlocking(false);
			selector = Selector.open();
			server.socket().bind(new InetSocketAddress(port));
			System.out.println("Server running on "
					+ server.socket().getInetAddress() + ":" + port);
		} catch (IOException e) {
			System.out.println("Trying " + (port + 1));
			this.port = port + 1;
			initServer(this.port);
		}
	}

	@Override
	public void run() {
		serve();
	}

	synchronized public void diffuse(String message) {
		// TODO
	}

	private void serve() {

		try {
			server.register(selector, SelectionKey.OP_ACCEPT);
		} catch (ClosedChannelException e1) {
			System.err
					.println("server.register(selector, SelectionKey.OP_ACCEPT) : "
							+ e1);
		}

		String old[] = new String[] {};
		String last = "";
		while (true) {

			int readyChannels;
			try {
				readyChannels = selector.select();

				if (readyChannels == 0)
					continue;
			} catch (IOException e) {
				System.out.println("selector.select() : " + e);
			}

			Set<SelectionKey> selectedKeys = selector.selectedKeys();

			Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

			while (keyIterator.hasNext()) {

				SelectionKey key = keyIterator.next();

				if (key.isAcceptable()) {
					try {
						ServerSocketChannel channel = (ServerSocketChannel) key
								.channel();
						channel.configureBlocking(false);
						SocketChannel sc = channel.accept();
						if (sc != null) {
							sc.configureBlocking(false);
							sc.register(selector, SelectionKey.OP_READ,
									SelectionKey.OP_WRITE);
							System.out.println("Nouvelle connexion");
						}
					} catch (IOException ie) {
						System.err
								.println("sc.register(selector, SelectionKey.OP_READ, SelectionKey.OP_WRITE) : "
										+ ie);
					}
				} else if (key.isConnectable()) {
					System.err.println("key.isConnectable()");
				} else if (key.isReadable()) {
					try {
						ByteBuffer bb = ByteBuffer.allocate(BUFFER_SIZE);
						SocketChannel sc = (SocketChannel) key.channel();
						sc.configureBlocking(false);
						int k = 0;
						k = sc.read(bb);
						if (k == -1) {
							remove(sc);
							sc.close();
							System.err.println("One client has got out!");
						}
						bb.flip();
						if (k > 0) {
							CharBuffer chars = CONVERTER.decode(bb);
							String rawPacket = last + chars.toString();
							if (rawPacket.contains(Packet.PACKET_DELIMITER)) {
								String tab[] = rawPacket
										.split(Packet.PACKET_DELIMITER);
								old = Arrays
										.copyOfRange(tab, 0, tab.length - 1);
								for (String ps : old) {
									Packet packet = Packet.buildPacketFrom(ps);
									switch (packet.getType()) {
									case JOIN_GROUP:
										handleJoinGroup(packet, sc);
										break;
									default:
										sendToManager(packet);
										break;
									}
								}
								old = new String[] {};
								last = tab[tab.length - 1];
								if (rawPacket.endsWith(Packet.PACKET_DELIMITER)) {
									Packet packet = Packet
											.buildPacketFrom(last);
									switch (packet.getType()) {
									case JOIN_GROUP:
										handleJoinGroup(packet, sc);
										break;
									default:
										sendToManager(packet);
										break;
									}
									last = "";
								}
							} else if (rawPacket
									.endsWith(Packet.PACKET_DELIMITER)) {
								String tab[] = rawPacket
										.split(Packet.PACKET_DELIMITER);
								for (String ps : tab) {
									Packet packet = Packet.buildPacketFrom(ps);
									switch (packet.getType()) {
									case JOIN_GROUP:
										handleJoinGroup(packet, sc);
										break;
									default:
										sendToManager(packet);
										break;
									}
								}
								old = new String[] {};
								continue;
							}
						}
					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
					}

				} else if (key.isWritable()) {

				}
				keyIterator.remove();
			}
		}

	}

	/**
	 * 
	 * @param packet
	 * @param sc
	 */
	private void handleJoinGroup(Packet packet, SocketChannel sc) {
		String managerId = packet.getManagerId();
		int capacity = 2;
		if ("".equalsIgnoreCase(managerId)) {
			managerId = new Date().getTime() + "";
			capacity = Integer.parseInt(packet.getContent());
			if (capacity < 2)
				capacity = 2;
			PlayerGroup playerGroup = new PlayerGroup(capacity);
			playerGroup.setId(managerId);
			PlayerServer player = new PlayerServer(packet.getSource(), sc);
			playerGroup.add(player);
			Packet pm = new ManagerIdPacket("server", player.getName(),
					managerId);
			System.out.println("Setting " + pm);
			player.send(pm);
			groups.put(managerId, playerGroup);
		} else {
			PlayerGroup playerGroup = groups.get(managerId);
			if (playerGroup == null) {
				capacity = Integer.parseInt(packet.getContent());
				if (capacity < 2)
					capacity = 2;
				playerGroup = new PlayerGroup(capacity);
				PlayerServer player = new PlayerServer(packet.getSource(), sc);
				playerGroup.add(player);
				playerGroup.setId(managerId);
				groups.put(managerId, playerGroup);
			} else {
				PlayerServer player = new PlayerServer(packet.getSource(), sc);
				int added = playerGroup.add(player);
				if (added == PlayerGroup.WAITING) {
					player.send(new WaitingPacket(managerId, player.getName(),
							managerId));
				} else if (added == PlayerGroup.EXIST) {
					player.send(new ExistPacket(managerId, player.getName(),
							managerId));
					String name = packet.getSource()
							+ System.currentTimeMillis();
					player.send(new NamePacket(managerId, player.getName(),
							name, managerId));
					handleJoinGroup(
							new JoinGroupPacket(name, packet.getDestination(),
									packet.getContentAsInteger(), managerId),
							sc);
				} else if (added == PlayerGroup.FULL) {
					player.send(new FullPacket(managerId, player.getName(),
							managerId));
				}
			}
		}
	}

	/**
	 * 
	 * @param sc
	 */
	private synchronized void remove(SocketChannel sc) {
		for (PlayerGroup pg : groups.values()) {
			pg.remove(sc);
		}
	}

	/**
	 * @param sc
	 * @param packet
	 */
	private void sendToManager(Packet packet) {
		try {
			String managerId = packet.getManagerId();
			PlayerGroup pg = groups.get(managerId);
			if (pg != null) {
				pg.receive(packet);
			} else {
				System.err.println(groups);
				System.err.println(packet);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void control(String cmd) {
		if (cmd == null)
			return;
		String[] tab = cmd.split(" ", 2);
		try {
			PacketType type = PacketType.valueOf(tab[0].toUpperCase());
			if (type != null) {
				switch (type) {
				case ASK:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new AskPacket("server", "cheick", "manager"));
					}
					break;
				case ASKED:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new AskedPacket("server", "cheick", "pique",
								"manager"));
					}
					break;
				case JOIN_GROUP:

					break;
				case CONNECTION:
					break;
				case FAULT:

					break;
				case GO_AROUND:

					break;
				case MESSAGE:
					if (tab.length > 1) {
						for (PlayerGroup pg : groups.values()) {
							pg.receive(new MessagePacket("server", "*ALL*",
									tab[1], "manager"));
						}
					}
					break;
				case PLAYED:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new PlayedPacket("server", "cheick",
								"[1,coeur];[1,carreau];[1,trefle]", "manager"));
					}
					break;

				case PLAY:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new PlayPacket("server", "cheick",
								"[1,pique]", "manager"));
					}
					break;
				case TAKE:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new TookPacket("server", "cheick",
								"[8,carreau];[8,trefle]", "manager"));
					}
					break;
				case TOOK:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new TookPacket("server", "cheick",
								"[8,carreau];[8,trefle]", "manager"));
					}
					break;
				case SET_CARDS:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new CardsPacket(pg.getId(), "cheick", "",
								"manager"));
					}

					break;
				case SET_NAME:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new NamePacket("source", "cheick", tab[1],
								"manager"));
					}
					break;

				default:

					if (cmd.equalsIgnoreCase("exit")) {
						System.out.println("Goodbye!");
						System.exit(0);
					} else {
						System.out.println("Commande non reconnue : " + tab[0]);
					}
					break;
				}

			}
		} catch (Exception e) {
			if (cmd.equalsIgnoreCase("exit")) {
				System.out.println("Goodbye!");
				System.exit(0);
			} else {
				System.err.println(e);
				System.out.println("Commande non reconnue : " + tab[0]);
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Server s = new Server(7000);
			new Thread(s).start();
			System.out.print("Command : ");
			String cmd = Keyboard.readString();
			while (cmd != null) {
				s.control(cmd);
				System.out.print("Command : ");
				cmd = Keyboard.readString();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Packet packet = new JoinGroupPacket("cheick", "server", 0,
		// "rien du tout");
		// System.out.println(packet.getContent(1));

	}
}
