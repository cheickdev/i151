/**
 * 
 */
package server;

import static packets.Packet.BUFFER_SIZE;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import packets.Packet;
import packets.PacketType;
import packets.i151.AskPacket;
import packets.i151.AskedPacket;
import packets.i151.MessagePacket;
import packets.i151.PlayedPacket;
import packets.i151.TookPacket;
import shared.PlayerGroup;

import common.Keyboard;

/**
 * @author Sissoko
 * @date 21 avr. 2014 19:08:10
 */
public class TestServer implements Runnable {
	public static int createdGroup = -1;
	protected static final Charset CONVERTER = Charset.availableCharsets().get(
			"UTF-8");
	public Map<String, PlayerGroup> groups = new HashMap<String, PlayerGroup>();
	ServerSocketChannel server;
	Selector selector;
	int port;

	/**
	 * @param port
	 * @throws IOException
	 */
	public TestServer(int port) throws IOException {
		this.port = port;
		initServer(port);
	}

	/**
	 * 
	 * @param port
	 */
	void initServer(int port) {
		try {
			server = ServerSocketChannel.open();
			server.configureBlocking(false);
			selector = Selector.open();
			server.socket().bind(new InetSocketAddress(port));
			System.out.println("Server running on "
					+ server.socket().getInetAddress() + ":" + port);
		} catch (IOException e) {
			System.out.println("Trying " + (port + 1));
			this.port = port + 1;
			initServer(this.port);
		}
	}

	private void control(String cmd) {
		if (cmd == null)
			return;
		String[] tab = cmd.split(" ", 2);
		try {
			PacketType type = PacketType.valueOf(tab[0].toUpperCase());
			if (type != null) {
				switch (type) {
				case ASK:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new AskPacket("server", "cheick", "manager"));
					}
					break;
				case ASKED:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new AskedPacket("server", "cheick", "pique",
								"manager"));
					}
					break;
				case JOIN_GROUP:

					break;
				case CONNECTION:
					break;
				case FAULT:

					break;
				case GO_AROUND:

					break;
				case MESSAGE:
					if (tab.length > 1) {
						for (PlayerGroup pg : groups.values()) {
							pg.receive(new MessagePacket("server", "*ALL*",
									tab[1], "manager"));
						}
					}
					break;
				case PLAYED:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new PlayedPacket("server", "cheick",
								"[1,coeur];[1,carreau];[1,trefle]", "manager"));
					}
					break;
				case TAKE:

					break;
				case TOOK:
					for (PlayerGroup pg : groups.values()) {
						pg.receive(new TookPacket("server", "cheick",
								"[8,carreau];[8,trefle]", "manager"));
					}
					break;

				default:
					if (cmd.equalsIgnoreCase("exit")) {
						System.out.println("Goodbye!");
						System.exit(0);
					} else {
						System.out.println("Commande non reconnue : " + tab[0]);
					}
					break;
				}

			}
		} catch (Exception e) {
			if (cmd.equalsIgnoreCase("exit")) {
				System.out.println("Goodbye!");
				System.exit(0);
			} else {
				System.err.println(e);
				System.out.println("Commande non reconnue : " + tab[0]);
			}
		}
	}

	private void serve() {

		try {
			server.register(selector, SelectionKey.OP_ACCEPT);
		} catch (ClosedChannelException e1) {
			System.err
					.println("server.register(selector, SelectionKey.OP_ACCEPT) : "
							+ e1);
		}

		while (true) {

			int readyChannels;
			try {
				readyChannels = selector.select();

				if (readyChannels == 0)
					continue;
			} catch (IOException e) {
				System.out.println("selector.select() : " + e);
			}

			Set<SelectionKey> selectedKeys = selector.selectedKeys();

			Iterator<SelectionKey> keyIterator = selectedKeys.iterator();

			while (keyIterator.hasNext()) {

				SelectionKey key = keyIterator.next();

				if (key.isAcceptable()) {
					try {
						ServerSocketChannel channel = (ServerSocketChannel) key
								.channel();
						channel.configureBlocking(false);
						SocketChannel sc = channel.accept();
						if (sc != null) {
							sc.configureBlocking(false);
							sc.register(selector, SelectionKey.OP_READ,
									SelectionKey.OP_WRITE);
							System.out.println("Nouvelle connexion");
						}
					} catch (IOException ie) {
						System.err
								.println("sc.register(selector, SelectionKey.OP_READ, SelectionKey.OP_WRITE) : "
										+ ie);
					}
				} else if (key.isConnectable()) {
					System.err.println("key.isConnectable()");
				} else if (key.isReadable()) {
					try {
						ByteBuffer bb = ByteBuffer.allocate(BUFFER_SIZE);
						SocketChannel sc = (SocketChannel) key.channel();
						sc.configureBlocking(false);
						int k = 0;
						k = sc.read(bb);
						if (k == -1) {
							// remove(sc);
							sc.close();
							System.err.println("One client has got out!");
						}
						bb.flip();
						if (k > 0) {
							CharBuffer chars = CONVERTER.decode(bb);
							String rawPacket = chars.toString();
							if (rawPacket.length() > 0) {
								Packet packet = Packet
										.buildPacketFrom(rawPacket);
								System.out.println("Cote serveur : " + packet);
								// if (packet.getType() == PacketType.TAKE) {
								// sc.write(ByteBuffer.wrap(new TookPacket(
								// "server", packet.getSource(),
								// "[1trefle]").getEncoding()
								// .getBytes()));
								// } else {
								// sc.write(ByteBuffer.wrap(rawPacket
								// .getBytes()));
								// }
							}
						}
					} catch (Exception e) {
						System.out.println(e);
						e.printStackTrace();
					}

				} else if (key.isWritable()) {

				}
				keyIterator.remove();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		serve();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			TestServer s = new TestServer(65537);
			new Thread(s).start();
			System.out.print("Command : ");
			String cmd = Keyboard.readString();
			while (cmd != null) {
				s.control(cmd);
				System.out.print("Command : ");
				cmd = Keyboard.readString();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
