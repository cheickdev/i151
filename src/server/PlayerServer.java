/**
 * 
 */
package server;

import graphic.HumanHand;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import packets.Packet;
import packets.i151.AskPacket;
import packets.i151.PlayPacket;
import shared.Card;
import shared.Player;

/**
 * @author sissoko
 * @date 22 déc. 2012 19:18:54
 */
public class PlayerServer extends Player {

	public SocketChannel socket;

	/**
	 * 
	 */

	public PlayerServer(String name, SocketChannel socket) {
		super(name);
		this.socket = socket;
		hand = new HumanHand();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#ask(java.lang.String)
	 */
	@Override
	public void ask() {
		send(new AskPacket(managerId, name, managerId));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#play(shared.Card)
	 */
	@Override
	public void play(Card top) {
		send(new PlayPacket(managerId, name, Card.encode(top), managerId));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#play(java.lang.String)
	 */
	@Override
	public void play(String ask) {
		send(new PlayPacket(managerId, name, ask, managerId));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#send(packets.Packet)
	 */
	@Override
	public void send(Packet packet) {
		try {
			socket.write(ByteBuffer.wrap(packet.getEncoding().getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#receive(packets.Packet)
	 */
	@Override
	public void receive(Packet packet) {
		if (packet != null) {
			try {
				socket.write(ByteBuffer.wrap(packet.getEncoding().getBytes()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#connect(java.lang.String, int, java.lang.String)
	 */
	@Override
	public boolean connect(String host, int port, String managerId) {
		throw new UnsupportedOperationException();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shared.Player#askBegin()
	 */
	@Override
	synchronized public boolean askBegin() {
		while (!beginAnswered) {
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return begin;
	}

	/**
	 * @param begin
	 *            the begin to set
	 */
	synchronized public void setBegin(boolean begin) {
		super.setBegin(begin);
		notifyAll();
	}
}
